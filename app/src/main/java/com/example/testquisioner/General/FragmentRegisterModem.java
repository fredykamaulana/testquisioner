package com.example.testquisioner.General;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.testquisioner.FTTH_Main.FTTH_Partnership.FragmentRG;
import com.example.testquisioner.FTTH_Main.FragmentONT;
import com.example.testquisioner.FTTH_Main.FragmentSTB4K;
import com.example.testquisioner.FTTH_Main.FragmentSTBLiteFTTH;
import com.example.testquisioner.HFC.FragmentModemD2Wifi;
import com.example.testquisioner.HFC.FragmentModemD2Wired;
import com.example.testquisioner.HFC.FragmentModemD3Wifi;
import com.example.testquisioner.HFC.FragmentModemD3Wired;
import com.example.testquisioner.HFC.FragmentSTBLite;
import com.example.testquisioner.HFC.FragmentSmartbox;
import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.Model.ModelFTTH;
import com.example.testquisioner.Model.ModelFTTH_P;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;

import java.io.Serializable;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentRegisterModem extends Fragment {
    private Button btnNext, btnBack;

    private ModelHFC model;
    private ModelFTTH modelFTTH;
    private ModelFTTH_P modelFTTH_p;
    private String SELECTED_NET = "";
    private String SELECTED_DEVICE = "";
    private String SELECTED_TYPE = "";

    public FragmentRegisterModem() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_modem, container, false);
        getActivity().setTitle("Register Modem");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        SELECTED_NET = bundle.getString("net");
        if (bundle != null) {
            if (SELECTED_NET.equals("HFC")){
                model = (ModelHFC) bundle.getSerializable("bundle");
                SELECTED_NET = model.getNetwork();
                SELECTED_DEVICE = model.getDevice();
                SELECTED_TYPE = model.getType();
            }else if (SELECTED_NET.equals("FTTH")){
                modelFTTH = (ModelFTTH) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH.getNetwork();
                SELECTED_DEVICE = modelFTTH.getDevice();
                SELECTED_TYPE = modelFTTH.getType();
            }else if (SELECTED_NET.equals("FTTH-Partnership")){
                modelFTTH_p = (ModelFTTH_P) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH_p.getNetwork();
                SELECTED_DEVICE = modelFTTH_p.getDevice();
                SELECTED_TYPE = modelFTTH_p.getType();
            }
        } else { }

        btnNext = v.findViewById(R.id.btn_next_reg_modem);
        if (SELECTED_NET.equals("HFC")){
            nextAction(R.id.ll_fragment_frame_hfc, new FragmentCheckKonesi(), model, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH")){
            nextAction(R.id.ll_fragment_frame_ftth, new FragmentCheckKonesi(), modelFTTH, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentCheckKonesi(), modelFTTH_p, SELECTED_NET);
        }else { }

        btnBack = v.findViewById(R.id.btn_back_reg_modem);
        if (SELECTED_NET.equals("HFC")){
            if (SELECTED_TYPE.equals("Modem D3 Wifi")) {
                backAction(R.id.ll_fragment_frame_hfc, new FragmentModemD3Wifi(), model, SELECTED_NET);
            } else if (SELECTED_TYPE.equals("Modem D3 Wired")) {
                backAction(R.id.ll_fragment_frame_hfc, new FragmentModemD3Wired(), model, SELECTED_NET);
            } else if (SELECTED_TYPE.equals("Modem D2 Wifi")) {
                backAction(R.id.ll_fragment_frame_hfc, new FragmentModemD2Wifi(), model, SELECTED_NET);
            } else if (SELECTED_TYPE.equals("Modem D2 Wired")) {
                backAction(R.id.ll_fragment_frame_hfc, new FragmentModemD2Wired(), model, SELECTED_NET);
            } else if (SELECTED_TYPE.equals("Smartbox")) {
                backAction(R.id.ll_fragment_frame_hfc, new FragmentSmartbox(), model, SELECTED_NET);
            } else if (SELECTED_TYPE.equals("STB Lite")) {
                backAction(R.id.ll_fragment_frame_hfc, new FragmentSTBLite(), model, SELECTED_NET);
            } else { }
        }else if (SELECTED_NET.equals("FTTH")){
            if (SELECTED_DEVICE.equals("ONT")) {
                backAction(R.id.ll_fragment_frame_ftth, new FragmentONT(), modelFTTH, SELECTED_NET);
            } else if (SELECTED_DEVICE.equals("STB 4K")) {
                backAction(R.id.ll_fragment_frame_ftth, new FragmentSTB4K(), modelFTTH, SELECTED_NET);
            } else if (SELECTED_DEVICE.equals("STB Lite")) {
                backAction(R.id.ll_fragment_frame_ftth, new FragmentSTBLiteFTTH(), modelFTTH, SELECTED_NET);
            } else { }
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            if (SELECTED_DEVICE.equals("RG")) {
                backAction(R.id.ll_fragment_frame_ftth_p, new FragmentRG(), modelFTTH_p, SELECTED_NET);
            } else if (SELECTED_DEVICE.equals("STB 4K")) {
                backAction(R.id.ll_fragment_frame_ftth_p, new FragmentSTB4K(), modelFTTH_p, SELECTED_NET);
            } else if (SELECTED_DEVICE.equals("STB Lite")) {
                backAction(R.id.ll_fragment_frame_ftth_p, new FragmentSTBLiteFTTH(), modelFTTH_p, SELECTED_NET);
            } else { }
        }else { }
    }

    private void backAction(final int from, final Fragment to, final Serializable m, final String s) {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(to, "bundle", m);
                fh.goToFragment(from, to, getFragmentManager());
            }
        });
    }

    private void nextAction(final int from, final Fragment to, final Serializable m, final String s){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(to, "bundle", m);
                fh.goToFragment(from, to, getFragmentManager());
            }
        });
    }
}
