package com.example.testquisioner.General;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.testquisioner.FTTH_Main.FTTH.MainActivityFTTH;
import com.example.testquisioner.FTTH_Main.FTTH_Partnership.MainActivityFTTH_P;
import com.example.testquisioner.HFC.MainActivityHFC;
import com.example.testquisioner.Model.ModelFTTH;
import com.example.testquisioner.Model.ModelFTTH_P;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;

import java.io.Serializable;

public class MainActivityNetwork extends AppCompatActivity {
    private Spinner spNetwork;
    private Button btnNext, btnBack;
    private LinearLayout llsetNetwork;

    public String SELECTED_NET = "";
    private ModelHFC modelHFC;
    private ModelFTTH modelFTTH;
    private ModelFTTH_P modelFTTH_p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_network);
        setTitle("Pilih Jaringan");
        setupView();
    }

    private void setupView() {
        modelHFC = new ModelHFC();
        modelFTTH = new ModelFTTH();
        modelFTTH_p = new ModelFTTH_P();

        ArrayAdapter netAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.network));
        netAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spNetwork = findViewById(R.id.sp_network);
        spNetwork.setAdapter(netAdapter);
        spNetwork.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_NET = spNetwork.getSelectedItem().toString();

                if (SELECTED_NET.equals("HFC")){
                    modelHFC.setNetwork(SELECTED_NET);
                    nextAction(MainActivityHFC.class, modelHFC);
                }else if (SELECTED_NET.equals("FTTH")){
                    modelFTTH.setNetwork(SELECTED_NET);
                    nextAction(MainActivityFTTH.class, modelFTTH);
                }else if (SELECTED_NET.equals("FTTH-Partnership")){
                    modelFTTH_p.setNetwork(SELECTED_NET);
                    nextAction(MainActivityFTTH_P.class, modelFTTH_p);
                }else { }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnNext = findViewById(R.id.btn_next_network);
        btnBack = findViewById(R.id.btn_back_network);
        llsetNetwork = findViewById(R.id.ll_set_network);
    }

    public void nextAction(final Class<?> c, final Serializable m){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),c);
                intent.putExtra("bundle", m);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
