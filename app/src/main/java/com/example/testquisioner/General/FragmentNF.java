package com.example.testquisioner.General;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.Model.ModelFTTH;
import com.example.testquisioner.Model.ModelFTTH_P;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;

import java.io.Serializable;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNF extends Fragment {
    private Button btnNext, btnBack;

    private ModelHFC model;
    private ModelFTTH modelFTTH;
    private ModelFTTH_P modelFTTH_p;

    private String SELECTED_NET = "";

    public FragmentNF() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nf, container, false);
        getActivity().setTitle("Halaman NF");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        if (bundle.getString("net").equals("HFC")){
            if (bundle != null){
                model = (ModelHFC) bundle.getSerializable("bundle");
                SELECTED_NET = model.getNetwork();
            }else { }
        }else if (bundle.getString("net").equals("FTTH")){
            if (bundle != null){
                modelFTTH = (ModelFTTH) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH.getNetwork();
            }else { }
        }else if (bundle.getString("net").equals("FTTH-Partnership")){
            if (bundle != null){
                modelFTTH_p = (ModelFTTH_P) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH_p.getNetwork();
            }else { }
        }

        btnNext = v.findViewById(R.id.btn_next_nf);
        if (SELECTED_NET.equals("HFC")){
            nextAction(R.id.ll_fragment_frame_hfc, new FragmentSubmitReport(), model, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH")){
            nextAction(R.id.ll_fragment_frame_ftth, new FragmentSubmitReport(), modelFTTH, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentSubmitReport(), modelFTTH_p, SELECTED_NET);
        }else { }

        btnBack = v.findViewById(R.id.btn_back_nf);
        if (SELECTED_NET.equals("HFC")){
            backAction(R.id.ll_fragment_frame_hfc, new FragmentRouter(), model, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH")){
            backAction(R.id.ll_fragment_frame_ftth, new FragmentRouter(), modelFTTH, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            backAction(R.id.ll_fragment_frame_ftth_p, new FragmentRouter(), modelFTTH_p, SELECTED_NET);
        }else { }
    }

    private void backAction(final int from, final Fragment to, final Serializable m, final String s){
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(to, "bundle", m);
                fh.goToFragment(from, to, getFragmentManager());
            }
        });
    }

    private void nextAction(final int from, final Fragment to, final Serializable m, final String s){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(to, "bundle", m);
                fh.goToFragment(from, to, getFragmentManager());
            }
        });
    }
}
