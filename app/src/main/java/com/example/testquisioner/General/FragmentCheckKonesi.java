package com.example.testquisioner.General;


import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.testquisioner.Helper.DialogHelper;
import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.Model.ModelFTTH;
import com.example.testquisioner.Model.ModelFTTH_P;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;

import java.io.Serializable;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCheckKonesi extends Fragment {
    private Button btnNext, btnBack;

    private ModelHFC model;
    private ModelFTTH modelFTTH;
    private ModelFTTH_P modelFTTH_p;

    private String SELECTED_NET = "";

    public FragmentCheckKonesi() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_check_konesi, container, false);
        getActivity().setTitle("Check Koneksi");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        SELECTED_NET = bundle.getString("net");
        if (bundle != null) {
            if (SELECTED_NET.equals("HFC")){
                model = (ModelHFC) bundle.getSerializable("bundle");
                SELECTED_NET = model.getNetwork();
            }else if (SELECTED_NET.equals("FTTH")){
                modelFTTH = (ModelFTTH) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH.getNetwork();
            }else if (SELECTED_NET.equals("FTTH-Partnership")){
                modelFTTH_p = (ModelFTTH_P) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH_p.getNetwork();
            }
        } else { }

        if (SELECTED_NET.equals("HFC")){
            startDialog("Apakah Koneksi Berfungsi ?", model, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH")){
            startDialog("Apakah Koneksi Berfungsi ?", modelFTTH, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            startDialog("Apakah Koneksi Berfungsi ?", modelFTTH_p, SELECTED_NET);
        }

        btnNext = v.findViewById(R.id.btn_next_ck);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SELECTED_NET.equals("HFC")){
                    startDialog("Apakah Koneksi Berfungsi ?", model, SELECTED_NET);
                }else if (SELECTED_NET.equals("FTTH")){
                    startDialog("Apakah Koneksi Berfungsi ?", modelFTTH, SELECTED_NET);
                }else if (SELECTED_NET.equals("FTTH-Partnership")){
                    startDialog("Apakah Koneksi Berfungsi ?", modelFTTH_p, SELECTED_NET);
                }
            }
        });
        btnBack = v.findViewById(R.id.btn_back_ck);
        if (SELECTED_NET.equals("HFC")){
            backAction(R.id.ll_fragment_frame_hfc, new FragmentRegisterModem(), model, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH")){
            backAction(R.id.ll_fragment_frame_ftth, new FragmentRegisterModem(), modelFTTH, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            backAction(R.id.ll_fragment_frame_ftth_p, new FragmentRegisterModem(), modelFTTH_p, SELECTED_NET);
        }else { }
    }

    private void startDialog(String s, final Serializable m, final String net) {
        DialogHelper dh = new DialogHelper();
        dh.setupDialog(new androidx.appcompat.app.AlertDialog.Builder(getContext()), s,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //posotive
                        if (net.equals("HFC")){
                            nextAction(R.id.ll_fragment_frame_hfc, new FragmentRouter(), m, net);
                        }else if (net.equals("FTTH")){
                            nextAction(R.id.ll_fragment_frame_ftth, new FragmentRouter(), m,net);
                        }else if (net.equals("FTTH-Partnership")){
                            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentRouter(), m, net);
                        }else { }
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //negative
                        dialogInterface.dismiss();
                    }
                });
    }

    private void backAction(final int from, final Fragment to, final Serializable m, final String s){
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(to, "bundle", m);
                fh.goToFragment(from, to, getFragmentManager());
            }
        });
    }

    private void nextAction(final int from, final Fragment to, final Serializable m, String s){
        FragmentHelper fh = new FragmentHelper();
        fh.NET = s;
        fh.extra(to, "bundle", m);
        fh.goToFragment(from, to, getFragmentManager());
    }
}
