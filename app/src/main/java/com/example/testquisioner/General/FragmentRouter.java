package com.example.testquisioner.General;


import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.testquisioner.Helper.DialogHelper;
import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.Helper.ValidateHelper;
import com.example.testquisioner.Model.ModelFTTH;
import com.example.testquisioner.Model.ModelFTTH_P;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentRouter extends Fragment {
    private Button btnNext, btnBack;
    private EditText etMerk, etSn, etSignal, etChannel24, etchannel50, etSpeed;
    private RadioGroup rgWifi;
    private RadioButton rbWifiBaik, rbWifiBuruk;

    private String SELECTED_NET = "";
    private String stMerk, stSn, stSignal, stChannel24, stchannel50, stSpeed, stWifi;
    private ModelHFC model;
    private ModelFTTH modelFTTH;
    private ModelFTTH_P modelFTTH_p;

    private List<EditText> texts = new ArrayList<>();
    private List<String> data = new ArrayList<>();

    public FragmentRouter() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_router, container, false);
        getActivity().setTitle("Setting Wifi Router");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        SELECTED_NET = bundle.getString("net");
        if (SELECTED_NET.equals("HFC")){
            if (bundle != null){
                model = (ModelHFC) bundle.getSerializable("bundle");
                SELECTED_NET = model.getNetwork();
                stMerk = model.getMerkTypeRouter();
                stSn = model.getSerialNumberRouter();
                stSignal = model.getPowerSignallRouter();
                stChannel24 = model.getBand24ChannelRouter();
                stchannel50 = model.getBand5ChannelRouter();
                stSpeed = model.getLinkSpeedRouter();
            }else { }
        }else if (SELECTED_NET.equals("FTTH")){
            if (bundle != null){
                modelFTTH = (ModelFTTH) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH.getNetwork();
                stMerk = modelFTTH.getMerkTypeRouter();
                stSn = modelFTTH.getSerialNumberRouter();
                stSignal = modelFTTH.getPowerSignallRouter();
                stChannel24 = modelFTTH.getBand24ChannelRouter();
                stchannel50 = modelFTTH.getBand5CahnnelRouter();
                stSpeed = modelFTTH.getLinkSpeedRouter();
            }else { }
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            if (bundle != null){
                modelFTTH_p = (ModelFTTH_P) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH_p.getNetwork();
                stMerk = modelFTTH_p.getMerkTypeRouter();
                stSn = modelFTTH_p.getSerialNumberRouter();
                stSignal = modelFTTH_p.getPowerSignallRouter();
                stChannel24 = modelFTTH_p.getBand24ChannelRouter();
                stchannel50 = modelFTTH_p.getBand5ChannelRouter();
                stSpeed = modelFTTH_p.getLinkSpeedRouter();
            }else { }
        }

        rgWifi = v.findViewById(R.id.rg_jangkauan_router);
        rbWifiBaik = v.findViewById(R.id.rb_baik_router);
        rbWifiBuruk = v.findViewById(R.id.rb_buruk_router);

        etMerk = v.findViewById(R.id.et_merk_router);
        etSn = v.findViewById(R.id.et_serial_number);
        etSignal = v.findViewById(R.id.et_signal_router);
        etChannel24 = v.findViewById(R.id.et_channel_2_4ghz_router);
        etchannel50 = v.findViewById(R.id.et_channel_5ghz_router);
        etSpeed = v.findViewById(R.id.et_link_speed_router);

        texts.add(etMerk);
        texts.add(etSignal);
        texts.add(etChannel24);
        texts.add(etSpeed);

        data.add(stMerk);
        data.add(stSn);
        data.add(stSignal);
        data.add(stChannel24);
        data.add(stchannel50);
        data.add(stSpeed);

        fetchData(data);

        btnNext = v.findViewById(R.id.btn_next_router);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int wifi = rgWifi.getCheckedRadioButtonId();
                if (    texts.get(0).getText().toString().isEmpty() ||
                        texts.get(1).getText().toString().isEmpty() ||
                        texts.get(2).getText().toString().isEmpty() ||
                        texts.get(3).getText().toString().isEmpty() ){
                    ValidateHelper vh = new ValidateHelper();
                    vh.validasi(texts);
                }else if (wifi <= 0 ){
                    rgWifi.requestFocus();
                    Toast.makeText(getContext(), "Jangkauan Wifi belum dicheck", Toast.LENGTH_SHORT).show();
                }else {
                    if (wifi == rbWifiBaik.getId()){
                        stWifi = rbWifiBaik.getText().toString();
                    }if (wifi == rbWifiBuruk.getId()){
                        stWifi = rbWifiBuruk.getText().toString(); }

                    if (SELECTED_NET.equals("HFC")){
                        model.setCoverageRouter(stWifi);
                        startDialog(model, texts);
                    }else if (SELECTED_NET.equals("FTTH")){
                        modelFTTH.setCoverageRouter(stWifi);
                        startDialog(modelFTTH, texts);
                    }else if (SELECTED_NET.equals("FTTH-Partnership")){
                        modelFTTH_p.setCoverageRouter(stWifi);
                        startDialog(modelFTTH_p, texts);
                    }
                }
            }
        });

        btnBack = v.findViewById(R.id.btn_back_router);
        if (SELECTED_NET.equals("HFC")){
            backAction(R.id.ll_fragment_frame_hfc, new FragmentCheckKonesi(), model, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH")){
            backAction(R.id.ll_fragment_frame_ftth, new FragmentCheckKonesi(), modelFTTH, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            backAction(R.id.ll_fragment_frame_ftth_p, new FragmentCheckKonesi(), modelFTTH_p, SELECTED_NET);
        }else { }
    }

    private void startDialog(final Serializable m, final List<EditText> edt){
        DialogHelper dh = new DialogHelper();
        dh.setupDialog(new AlertDialog.Builder(getContext()), "Indikasi NF ?",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //positive
                        //go to NF
                        if (SELECTED_NET.equals("HFC")){
                            model.setNf("y");
                            model.setMerkTypeRouter(edt.get(0).getText().toString());
                            model.setSerialNumberRouter(etSn.getText().toString());
                            model.setPowerSignallRouter(edt.get(1).getText().toString());
                            model.setBand24ChannelRouter(edt.get(2).getText().toString());
                            model.setBand5ChannelRouter(etchannel50.getText().toString());
                            model.setLinkSpeedRouter(edt.get(3).getText().toString());
                            model.setCoverageRouter(stWifi);
                            nextAction(R.id.ll_fragment_frame_hfc, new FragmentNF(), m, SELECTED_NET);
                        }else if (SELECTED_NET.equals("FTTH")){
                            modelFTTH.setNf("y");
                            modelFTTH.setMerkTypeRouter(edt.get(0).getText().toString());
                            modelFTTH.setSerialNumberRouter(etSn.getText().toString());
                            modelFTTH.setPowerSignallRouter(edt.get(1).getText().toString());
                            modelFTTH.setBand24ChannelRouter(edt.get(2).getText().toString());
                            modelFTTH.setBand5CahnnelRouter(etchannel50.getText().toString());
                            modelFTTH.setLinkSpeedRouter(edt.get(3).getText().toString());
                            modelFTTH.setCoverageRouter(stWifi);
                            nextAction(R.id.ll_fragment_frame_ftth, new FragmentNF(), m, SELECTED_NET);
                        }else if (SELECTED_NET.equals("FTTH-Partnership")){
                            modelFTTH_p.setNf("y");
                            modelFTTH_p.setMerkTypeRouter(edt.get(0).getText().toString());
                            modelFTTH_p.setSerialNumberRouter(etSn.getText().toString());
                            modelFTTH_p.setPowerSignallRouter(edt.get(1).getText().toString());
                            modelFTTH_p.setBand24ChannelRouter(edt.get(2).getText().toString());
                            modelFTTH_p.setBand5ChannelRouter(etchannel50.getText().toString());
                            modelFTTH_p.setLinkSpeedRouter(edt.get(3).getText().toString());
                            modelFTTH_p.setCoverageRouter(stWifi);
                            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentNF(), m, SELECTED_NET);
                        }else { }
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //negative
                        //go to submit report
                        if (SELECTED_NET.equals("HFC")){
                            model.setNf("n");
                            nextAction(R.id.ll_fragment_frame_hfc, new FragmentSubmitReport(), m, SELECTED_NET);
                        }else if (SELECTED_NET.equals("FTTH")){
                            modelFTTH.setNf("n");
                            nextAction(R.id.ll_fragment_frame_ftth, new FragmentSubmitReport(), m, SELECTED_NET);
                        }else if (SELECTED_NET.equals("FTTH-Partnership")){
                            modelFTTH_p.setNf("n");
                            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentSubmitReport(), m, SELECTED_NET);
                        }else { }
                    }
                });
    }

    private void nextAction(final int from, final Fragment to, final Serializable m, String s){
        FragmentHelper fh = new FragmentHelper();
        fh.NET = s;
        fh.extra(to, "bundle", m);
        fh.goToFragment(from, to, getFragmentManager());
    }

    private void backAction(final int from, final Fragment to, final Serializable m, final String s){
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(to, "bundle", m);
                fh.goToFragment(from, to, getFragmentManager());
            }
        });
    }

    private void fetchData(List<String> str){
        for (int i = 0; i < str.size(); i++) {
            if (!str.isEmpty()){
                etMerk.setText(str.get(0));
                etSn.setText(str.get(1));
                etSignal.setText(str.get(2));
                etChannel24.setText(str.get(3));
                etchannel50.setText(str.get(4));
                etSpeed.setText(str.get(5));
            }else { }
        }
    }
}
