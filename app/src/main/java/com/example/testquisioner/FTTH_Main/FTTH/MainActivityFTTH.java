package com.example.testquisioner.FTTH_Main.FTTH;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.testquisioner.FTTH_Main.FragmentONT;
import com.example.testquisioner.FTTH_Main.FragmentSTB4K;
import com.example.testquisioner.FTTH_Main.FragmentSTBLiteFTTH;
import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.General.MainActivityNetwork;
import com.example.testquisioner.Model.ModelFTTH;
import com.example.testquisioner.R;

import java.io.Serializable;

public class MainActivityFTTH extends AppCompatActivity {
    private Button btnNext, btnBack;
    private LinearLayout llDevice;
    private Spinner spDevice;

    private ModelFTTH model;
    private String SELECTED_DEVICE = "";
    private String SELECTED_NET = "";
    private int DIALOG_COUNT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_ftth);
        setTitle("Jaringan FTTH");
        setupView();
        startDialog();
    }

    private void setupView() {
        final Intent intent = getIntent();
        if (intent != null){
            model = (ModelFTTH) intent.getSerializableExtra("bundle");
            SELECTED_NET = model.getNetwork();
        }

        llDevice = findViewById(R.id.ll_set_device_ftth);
        llDevice.setVisibility(View.GONE);

        btnNext = findViewById(R.id.btn_next_ftth);

        btnBack = findViewById(R.id.btn_back_ftth);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(MainActivityFTTH.this, MainActivityNetwork.class);
                main.putExtra("bundle", model);
                startActivity(main);
                finish();
            }
        });

        ArrayAdapter<String> adapterDevice = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.device_ftth));
        adapterDevice.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spDevice = findViewById(R.id.sp_device_ftth);
        spDevice.setAdapter(adapterDevice);
        spDevice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_DEVICE = spDevice.getSelectedItem().toString();
                model.setDevice(SELECTED_DEVICE);
                model.setType(null);
                model.setState("before");
                if (SELECTED_DEVICE.equals("ONT")) {
                    nextAction(new FragmentONT(), model, SELECTED_NET);
                } else if (SELECTED_DEVICE.equals("STB 4K")) {
                    nextAction(new FragmentSTB4K(), model, SELECTED_NET);
                } else if (SELECTED_DEVICE.equals("STB Lite")) {
                    nextAction(new FragmentSTBLiteFTTH(), model, SELECTED_NET);
                } else { }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void nextAction(final Fragment f, final Serializable m, final String s){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llDevice.setVisibility(View.GONE);
                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(f, "bundle", m);
                fh.goToFragment(R.id.ll_fragment_frame_ftth, f, getSupportFragmentManager());
            }
        });
    }

    private void startDialog(){
        DIALOG_COUNT++;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Apakah ONT/STB Menyala ?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                llDevice.setVisibility(View.VISIBLE);
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (DIALOG_COUNT == 1) {
                    giveAdvice("Silahkan Ganti Adaptor");
                } else if (DIALOG_COUNT == 2) {
                    giveAdvice("Silahkan Ganti ONT/STB");
                } else {
                }
            }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    private void giveAdvice(String s) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(s);
        builder.setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (DIALOG_COUNT < 2) {
                    startDialog();
                } else if (DIALOG_COUNT == 2) {
                    llDevice.setVisibility(View.VISIBLE);
                } else {
                }
            }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
