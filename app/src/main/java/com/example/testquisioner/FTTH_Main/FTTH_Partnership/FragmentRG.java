package com.example.testquisioner.FTTH_Main.FTTH_Partnership;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.testquisioner.FTTH_Main.FragmentFAT;
import com.example.testquisioner.FTTH_Main.FragmentONT;
import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.General.FragmentRegisterModem;
import com.example.testquisioner.Model.ModelFTTH_P;
import com.example.testquisioner.R;

import java.io.Serializable;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentRG extends Fragment {
    private Button btnNext, btnBack;
    private RadioGroup rgAdaptor, rg1000, rg2000, rg32, rgWifi;
    private RadioButton rbAdaptorBaik, rbAdaptorBuruk,
            rb1000Connected, rb1000Connecting,rb1000Disconnect,
            rb2000Connected, rb2000Connecting,rb2000Disconnect,
            rb32Connected, rb32Connecting, rb32Disconnect,
            rbWifiBaik, rbWifiBuruk;

    private String SELECTED_NET = "";
    private String STATE = "";
    private String stAdaptor, st1000, st2000, st32, stWifi;

    private ModelFTTH_P model;

    public FragmentRG() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rg, container, false);
        getActivity().setTitle("Check RG");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        final Bundle bundle = getArguments();
        if (bundle != null){
            model = (ModelFTTH_P) bundle.getSerializable("bundle");
            SELECTED_NET = model.getNetwork();
            STATE = model.getState();
        }else { }

        rgAdaptor = v.findViewById(R.id.rg_adaptor_rg);
        rbAdaptorBaik = v.findViewById(R.id.rb_baik_adaptor_rg);
        rbAdaptorBuruk = v.findViewById(R.id.rb_buruk_adaptor_rg);

        rg1000 = v.findViewById(R.id.rg_vlan_1000_rg);
        rb1000Connected = v.findViewById(R.id.rb_connected_vlan_1000_rg);
        rb1000Connecting = v.findViewById(R.id.rb_connecting_vlan_1000_rg);
        rb1000Disconnect = v.findViewById(R.id.rb_disconnect_vlan_1000_rg);

        rg2000 = v.findViewById(R.id.rg_vlan_2000_rg);
        rb2000Connected = v.findViewById(R.id.rb_connected_vlan_2000_rg);
        rb2000Connecting = v.findViewById(R.id.rb_connecting_vlan_2000_rg);
        rb2000Disconnect = v.findViewById(R.id.rb_disconnect_vlan_2000_rg);

        rg32 = v.findViewById(R.id.rg_vlan_32_rg);
        rb32Connected = v.findViewById(R.id.rb_connected_vlan_32_rg);
        rb32Connecting = v.findViewById(R.id.rb_connecting_vlan_32_rg);
        rb32Disconnect = v.findViewById(R.id.rb_disconnect_vlan_32_rg);

        rgWifi = v.findViewById(R.id.rg_wifi_rg);
        rbWifiBaik = v.findViewById(R.id.rb_baik_wifi_rg);
        rbWifiBuruk = v.findViewById(R.id.rb_buruk_wifi_rg);

        btnNext = v.findViewById(R.id.btn_next_rg);
        if (STATE.equals("before")){
            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentONT(), model, SELECTED_NET);
        }else if (STATE.equals("after")){
            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentRegisterModem(), model, SELECTED_NET);
        }else { }

        btnBack = v.findViewById(R.id.btn_back_rg);
        if (STATE.equals("before")){
            backAction(MainActivityFTTH_P.class, model);
        }else if (STATE.equals("after")){
            backFragment(R.id.ll_fragment_frame_ftth_p, new FragmentFAT(), model);
        }else { }
    }

    private void nextAction(final int from, final Fragment to, final Serializable m, final String s){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int adaptor = rgAdaptor.getCheckedRadioButtonId();
                int v1000 = rg1000.getCheckedRadioButtonId();
                int v2000 = rg2000.getCheckedRadioButtonId();
                int v32 = rg32.getCheckedRadioButtonId();
                int wifi = rgWifi.getCheckedRadioButtonId();
                if (adaptor <= 0){
                    rgAdaptor.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Adaptor Ethernet belum dicheck", Toast.LENGTH_SHORT).show();
                }else if (v1000 <= 0){
                    rg1000.requestFocus();
                    Toast.makeText(getContext(), "Status VLAN 1000 Ethernet belum dicheck", Toast.LENGTH_SHORT).show();
                }else if (v2000 <= 0){
                    rg2000.requestFocus();
                    Toast.makeText(getContext(), "Status VLAN 2000 Ethernet belum dicheck", Toast.LENGTH_SHORT).show();
                }else if (v32 <= 0){
                    rg32.requestFocus();
                    Toast.makeText(getContext(), "Status VLAN 32 Ethernet belum dicheck", Toast.LENGTH_SHORT).show();
                }else if (wifi <= 0){
                    rgWifi.requestFocus();
                    Toast.makeText(getContext(), "Jangkauan Wifi Ethernet belum dicheck", Toast.LENGTH_SHORT).show();
                }else {
                    if (adaptor == rbAdaptorBaik.getId()){
                        stAdaptor = rbAdaptorBaik.getText().toString();
                    }else if (adaptor == rbAdaptorBuruk.getId()){
                        stAdaptor = rbAdaptorBuruk.getText().toString(); }

                    if (v1000 == rb1000Connected.getId()){
                        st1000 = rb1000Connected.getText().toString();
                    }else if (v1000 == rb1000Connecting.getId()){
                        st1000 = rb1000Connecting.getText().toString();
                    }else  if (v1000 == rb1000Disconnect.getId()){
                        st1000 = rb1000Disconnect.getText().toString(); }

                    if (v2000 == rb2000Connected.getId()){
                        st2000 = rb2000Connected.getText().toString();
                    }else if (v2000 == rb2000Connecting.getId()){
                        st2000 = rb2000Connecting.getText().toString();
                    }else  if (v2000 == rb2000Disconnect.getId()){
                        st2000 = rb2000Disconnect.getText().toString(); }

                    if (v32 == rb32Connected.getId()){
                        st32 = rb32Connected.getText().toString();
                    }else if (v32 == rb32Connecting.getId()){
                        st32 = rb32Connecting.getText().toString();
                    }else  if (v32 == rb32Disconnect.getId()){
                        st32 = rb32Disconnect.getText().toString(); }

                    if (wifi == rbWifiBaik.getId()){
                        stWifi = rbWifiBaik.getText().toString();
                    }else if (wifi == rbWifiBuruk.getId()){
                        stWifi = rbWifiBuruk.getText().toString(); }

                    model.setEthernetRG(stAdaptor);
                    model.setVlan1000RG(st1000);
                    model.setVlan2000RG(st2000);
                    model.setVlan32RG(st32);
                    model.setCoverageWifiRG(stWifi);

                    FragmentHelper fh = new FragmentHelper();
                    fh.NET = s;
                    fh.extra(to, "bundle", m);
                    fh.goToFragment(from, to, getFragmentManager());
                }
            }
        });
    }

    private void backAction(final Class<?> c, final ModelFTTH_P m){
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),c);
                intent.putExtra("bundle", m);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    private void backFragment(final int from, final Fragment to, final ModelFTTH_P m){
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.NET = m.getNetwork();
                fh.extra(to, "bundle", m);
                fh.goToFragment(from, to, getFragmentManager());
            }
        });
    }
}
