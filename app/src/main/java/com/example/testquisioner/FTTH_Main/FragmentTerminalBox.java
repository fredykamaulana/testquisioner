package com.example.testquisioner.FTTH_Main;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.Model.ModelFTTH;
import com.example.testquisioner.Model.ModelFTTH_P;
import com.example.testquisioner.R;
import com.example.testquisioner.Helper.ValidateHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTerminalBox extends Fragment {
    private Button btnNext, btnBack;
    private RadioGroup rgDrop, rgSplicing, rgCord;
    private RadioButton rbDropBaik, rbDropBuruk, rbSplicingBaik, rbSplicingBuruk, rbCordBaik, rbCordBuruk;
    private EditText etSignal;

    private ModelFTTH model;
    private ModelFTTH_P modelFTTH_p;

    private String SELECTED_DEVICE = "";
    private String SELECTED_NET = "";
    private String stDrop, stSplicing, stCord, stSignal;

    private List<EditText> texts = new ArrayList<>();
    private List<String> data = new ArrayList<>();

    public FragmentTerminalBox() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_terminal_box, container, false);
        getActivity().setTitle("Check Terminal Box");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        SELECTED_NET = bundle.getString("net");
        if (SELECTED_NET.equals("FTTH")){
            if (bundle != null){
                model = (ModelFTTH) bundle.getSerializable("bundle");
                SELECTED_DEVICE = model.getDevice();
                SELECTED_NET = model.getNetwork();
                stSignal = model.getOutputSignalBox();
            }else { }
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            if (bundle != null){
                modelFTTH_p = (ModelFTTH_P) bundle.getSerializable("bundle");
                SELECTED_DEVICE = modelFTTH_p.getDevice();
                SELECTED_NET = modelFTTH_p.getNetwork();
                stSignal = modelFTTH_p.getOutputSignalBox();
            }else { }
        }

        rgDrop = v.findViewById(R.id.rg_drop_terminal_box_ftth);
        rbDropBaik = v.findViewById(R.id.rb_baik_drop_terminal_box_ftth);
        rbDropBuruk = v.findViewById(R.id.rb_buruk_drop_terminal_box_ftth);

        rgSplicing = v.findViewById(R.id.rg_splicing_terminal_box_ftth);
        rbSplicingBaik = v.findViewById(R.id.rb_baik_splicing_terminal_box_ftth);
        rbSplicingBuruk = v.findViewById(R.id.rb_buruk_splicing_terminal_box_ftth);

        rgCord = v.findViewById(R.id.rg_patch_terminal_box_ftth);
        rbCordBaik = v.findViewById(R.id.rb_baik_patch_terminal_box_ftth);
        rbCordBuruk = v.findViewById(R.id.rb_buruk_patch_terminal_box_ftth);

        etSignal = v.findViewById(R.id.et_signal_output_terminal_box);
        texts.add(etSignal);
        data.add(stSignal);

        fetchData(data);

        btnNext = v.findViewById(R.id.btn_next_terminal_box);
        if (SELECTED_NET.equals("FTTH")){
            nextAction(R.id.ll_fragment_frame_ftth, new FragmentFAT(), model, texts, SELECTED_NET);
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentFAT(), modelFTTH_p, texts, SELECTED_NET);
        }else { }

        btnBack = v.findViewById(R.id.btn_back_terminal_box);
        if (SELECTED_NET.equals("FTTH")){
            if (SELECTED_DEVICE.equals("ONT")){
                backAction(R.id.ll_fragment_frame_ftth, new FragmentONT(), model, SELECTED_NET);
            }else if (SELECTED_DEVICE.equals("STB 4K")){
                backAction(R.id.ll_fragment_frame_ftth, new FragmentSTB4K(), model, SELECTED_NET);
            }else if (SELECTED_DEVICE.equals("STB Lite")){
                backAction(R.id.ll_fragment_frame_ftth, new FragmentSTBLiteFTTH(), model, SELECTED_NET);
            }else { }
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            backAction(R.id.ll_fragment_frame_ftth_p, new FragmentONT(), modelFTTH_p, SELECTED_NET);
        }else { }
    }

    private void backAction(final int from, final Fragment f, final Serializable m, final String s){
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SELECTED_NET.equals("FTTH")){
                    model.setState("before");
                }else if (SELECTED_NET.equals("")){
                    modelFTTH_p.setState("before"); }

                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(f,"bundle", m);
                fh.goToFragment(from, f, getFragmentManager());
            }
        });
    }

    private void nextAction(final int from, final Fragment to, final Serializable m, final List<EditText> txt, final String s){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int drop = rgDrop.getCheckedRadioButtonId();
                int splicing = rgSplicing.getCheckedRadioButtonId();
                int cord = rgCord.getCheckedRadioButtonId();
                if (drop <= 0){
                    rgDrop.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Drop Wire belum dicheck", Toast.LENGTH_SHORT).show();
                }else if (splicing <= 0){
                    rgSplicing.requestFocus();
                    Toast.makeText(getContext(), "Kondisi hasil Splicing belum dicheck", Toast.LENGTH_SHORT).show();
                }else if (cord <= 0){
                    rgCord.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Patch Cord belum dicheck", Toast.LENGTH_SHORT).show();
                }else if (txt.get(0).getText().toString().isEmpty()){
                    ValidateHelper vh = new ValidateHelper();
                    vh.validasi(txt);
                }else {
                    if (drop == rbDropBaik.getId()){
                        stDrop = rbDropBaik.getText().toString();
                    }else if (drop == rbDropBuruk.getId()){
                        stDrop = rbDropBuruk.getText().toString(); }

                    if (splicing == rbSplicingBaik.getId()){
                        stSplicing = rbSplicingBaik.getText().toString();
                    }else if (splicing == rbSplicingBuruk.getId()){
                        stSplicing = rbSplicingBuruk.getText().toString(); }

                    if (cord == rbCordBaik.getId()){
                        stCord = rbCordBaik.getText().toString();
                    }else if (cord == rbCordBuruk.getId()){
                        stCord = rbCordBuruk.getText().toString(); }

                    if (SELECTED_NET.equals("FTTH")){
                        model.setPatchCordBox(stCord);
                        model.setSplicingBox(stSplicing);
                        model.setDropWireBox(stDrop);
                        model.setOutputSignalBox(txt.get(0).getText().toString());
                    }else if (SELECTED_NET.equals("FTTH-Partnership")){
                        modelFTTH_p.setPatchCordBox(stCord);
                        modelFTTH_p.setSplicingBox(stSplicing);
                        modelFTTH_p.setDropWireBox(stDrop);
                        modelFTTH_p.setOutputSignalBox(txt.get(0).getText().toString());
                    }
                    FragmentHelper fh = new FragmentHelper();
                    fh.NET = s;
                    fh.extra(to, "bundle", m);
                    fh.goToFragment(from, to, getFragmentManager());
                }
            }
        });
    }

    private void fetchData(List<String> str){
        for (int i = 0; i < str.size(); i++) {
            if (!str.isEmpty()){
                etSignal.setText(str.get(0)); } else { }
        }
    }
}
