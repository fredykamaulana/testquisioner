package com.example.testquisioner.FTTH_Main;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.testquisioner.Helper.DialogHelper;
import com.example.testquisioner.FTTH_Main.FTTH.MainActivityFTTH;
import com.example.testquisioner.FTTH_Main.FTTH_Partnership.FragmentRG;
import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.General.FragmentRegisterModem;
import com.example.testquisioner.Model.ModelFTTH;
import com.example.testquisioner.Model.ModelFTTH_P;
import com.example.testquisioner.R;
import com.example.testquisioner.Helper.ValidateHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentONT extends Fragment {
    private Button btnNext, btnBack;
    private EditText etOutput;
    private RadioGroup rgCord, rgAdaptor;
    private RadioButton rbCordBaik, rbCordBuruk, rbAdaptorBaik, rbAdaptorBuruk;

    private String SELECTED_NET = "";
    private String SELECTED_DEVICE = "";
    private String STATE = "";
    private String stCord, stAdaptor, stOutput;

    private ModelFTTH model;
    private ModelFTTH_P modelFTTH_p;

    private List<EditText> texts = new ArrayList<>();
    private List<String> data = new ArrayList<>();

    public FragmentONT() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ont, container, false);
        getActivity().setTitle("Check ONT");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        SELECTED_NET = bundle.getString("net");
        if (SELECTED_NET.equals("FTTH")){
            if (bundle != null) {
                model = (ModelFTTH) bundle.getSerializable("bundle");
                SELECTED_NET = model.getNetwork();
                SELECTED_DEVICE = model.getDevice();
                STATE = model.getState();
                stOutput = model.getOutputSignalONT();
            } else { }
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            if (bundle != null) {
                modelFTTH_p = (ModelFTTH_P) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH_p.getNetwork();
                SELECTED_DEVICE = modelFTTH_p.getDevice();
                STATE = modelFTTH_p.getState();
                stOutput = modelFTTH_p.getOutputSignalONT();
            } else { }
        }

        rgCord = v.findViewById(R.id.rg_patch_ont);
        rbCordBaik = v.findViewById(R.id.rb_baik_patch_ont);
        rbCordBuruk = v.findViewById(R.id.rb_buruk_patch_ont);

        rgAdaptor = v.findViewById(R.id.rg_adaptor_ont);
        rbAdaptorBaik = v.findViewById(R.id.rb_baik_adaptor_ont);
        rbAdaptorBuruk = v.findViewById(R.id.rb_buruk_adaptor_ont);

        etOutput = v.findViewById(R.id.et_signal_output_ont);
        texts.add(etOutput);
        data.add(stOutput);

        if (STATE.equals("before")) {
            fetchData(data);
        }else if (STATE.equals("after")){ }

        btnNext = v.findViewById(R.id.btn_next_ont);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int cord = rgCord.getCheckedRadioButtonId();
                int adaptor = rgAdaptor.getCheckedRadioButtonId();
                if (cord <= 0) {
                    rgCord.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Patc Cord belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (adaptor <= 0) {
                    rgAdaptor.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Adaptor Patc Cord belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (texts.get(0).getText().toString().isEmpty()) {
                    ValidateHelper vh = new ValidateHelper();
                    vh.validasi(texts);
                } else {
                    if (cord == rbCordBaik.getId()){
                        stCord = rbCordBaik.getText().toString();
                    }else if (cord == rbCordBuruk.getId()){
                        stCord = rbCordBuruk.getText().toString(); }

                    if (adaptor == rbAdaptorBaik.getId()){
                        stAdaptor = rbAdaptorBaik.getText().toString();
                    }else if (adaptor == rbAdaptorBuruk.getId()){
                        stAdaptor = rbAdaptorBuruk.getText().toString(); }

                    if (STATE.equals("before")) {
                        if (SELECTED_NET.equals("FTTH")){
                            startDialog(model, SELECTED_NET);
                        } else if (SELECTED_NET.equals("FTTH-Partnership")){
                            startDialog(modelFTTH_p, SELECTED_NET); }
                    } else if (STATE.equals("after")) {
                        if (SELECTED_NET.equals("FTTH")) {
                            model.setPatchCordONT(stCord);
                            model.setAdapterPatchCordONT(stAdaptor);
                            model.setOutputSignalONT(texts.get(0).getText().toString());
                            nextAction(R.id.ll_fragment_frame_ftth, new FragmentRegisterModem(), model, SELECTED_NET);
                        } else if (SELECTED_NET.equals("FTTH-Partnership")) {
                            modelFTTH_p.setPatchCordONT(stCord);
                            modelFTTH_p.setAdapterPatchCordONT(stAdaptor);
                            modelFTTH_p.setOutputSignalONT(texts.get(0).getText().toString());
                            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentRegisterModem(), modelFTTH_p, SELECTED_NET);
                        } else { }
                    }
                }
            }
        });

        btnBack = v.findViewById(R.id.btn_back_ont);
        if (STATE.equals("before")) {
            if (SELECTED_NET.equals("FTTH")) {
                backAction(MainActivityFTTH.class, model);
            } else if (SELECTED_NET.equals("FTTH-Partnership")) {
                if (SELECTED_DEVICE.equals("RG")) {
                    backFragment(R.id.ll_fragment_frame_ftth_p, new FragmentRG(), modelFTTH_p, SELECTED_NET);
                } else if (SELECTED_DEVICE.equals("STB 4K")) {
                    backFragment(R.id.ll_fragment_frame_ftth_p, new FragmentSTB4K(), modelFTTH_p, SELECTED_NET);
                } else if (SELECTED_DEVICE.equals("STB Lite")) {
                    backFragment(R.id.ll_fragment_frame_ftth_p, new FragmentSTBLiteFTTH(), modelFTTH_p, SELECTED_NET);
                }
            } else {
            }
        } else if (STATE.equals("after")) {
            if (SELECTED_NET.equals("FTTH")) {
                backFragment(R.id.ll_fragment_frame_ftth, new FragmentFAT(), model, SELECTED_NET);
            } else if (SELECTED_NET.equals("FTTH-Partnership")) {
                backFragment(R.id.ll_fragment_frame_ftth_p, new FragmentFAT(), modelFTTH_p, SELECTED_NET);
            } else {
            }
        } else {
        }

    }

    private void startDialog(final Serializable m, final String net) {
        DialogHelper dh = new DialogHelper();
        dh.setupDialog(new AlertDialog.Builder(getContext()), "Apakah Terminal Box bisa diakses ?",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //positive
                        if (net.equals("FTTH")) {
                            model.setCheckTerminalBox("y");
                            model.setPatchCordONT(stCord);
                            model.setAdapterPatchCordONT(stAdaptor);
                            model.setOutputSignalONT(texts.get(0).getText().toString());
                            nextAction(R.id.ll_fragment_frame_ftth, new FragmentTerminalBox(), m, net);
                        } else if (net.equals("FTTH-Partnership")) {
                            modelFTTH_p.setCheckTerminalBox("y");
                            modelFTTH_p.setPatchCordONT(stCord);
                            modelFTTH_p.setAdapterPatchCordONT(stAdaptor);
                            modelFTTH_p.setOutputSignalONT(texts.get(0).getText().toString());
                            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentTerminalBox(), m, net);
                        } else {
                        }
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //negative
                        //go to FAT
                        if (net.equals("FTTH")) {
                            model.setCheckTerminalBox("n");
                            model.setPatchCordONT(stCord);
                            model.setAdapterPatchCordONT(stAdaptor);
                            model.setOutputSignalONT(texts.get(0).getText().toString());
                            nextAction(R.id.ll_fragment_frame_ftth, new FragmentFAT(), m, net);
                        } else if (net.equals("FTTH-Partnership")) {
                            modelFTTH_p.setCheckTerminalBox("n");
                            modelFTTH_p.setPatchCordONT(stCord);
                            modelFTTH_p.setAdapterPatchCordONT(stAdaptor);
                            modelFTTH_p.setOutputSignalONT(texts.get(0).getText().toString());
                            nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentFAT(), m, net);
                        } else {
                        }
                    }
                });
    }

    private void backAction(final Class<?> c, final Serializable m) {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), c);
                intent.putExtra("bundle", m);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    private void backFragment(final int from, final Fragment to, final Serializable m, final String s) {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(to, "bundle", m);
                fh.goToFragment(from, to, getFragmentManager());
            }
        });
    }

    private void nextAction(final int from, final Fragment to, final Serializable m, String s) {
        FragmentHelper fh = new FragmentHelper();
        fh.NET = s;
        fh.extra(to, "bundle", m);
        fh.goToFragment(from, to, getFragmentManager());
    }

    private void fetchData(List<String> str){
        for (int i = 0; i < str.size(); i++) {
            if (!str.isEmpty()){
                etOutput.setText(str.get(0)); } else { }
        }
    }
}
