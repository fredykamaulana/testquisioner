package com.example.testquisioner.FTTH_Main;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.testquisioner.FTTH_Main.FTTH_Partnership.FragmentRG;
import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.Model.ModelFTTH;
import com.example.testquisioner.Model.ModelFTTH_P;
import com.example.testquisioner.R;
import com.example.testquisioner.Helper.ValidateHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFAT extends Fragment {
    private Button btnNext, btnBack;
    private EditText etNomor, etSignal;
    private RadioGroup rgKonektor;
    private RadioButton rbKonektorBaik, rbKonektorBuruk;

    private ModelFTTH model;
    private ModelFTTH_P modelFTTH_p;

    private String SELECTED_NET = "";
    private String SELECTED_DEVICE = "";
    private String TERMINAL = "";
    private String stkonektor, stNomor, stSignal;

    private List<EditText> texts = new ArrayList<>();
    private List<String> data = new ArrayList<>();

    public FragmentFAT() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fat, container, false);
        getActivity().setTitle("Check FAT");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        SELECTED_NET = bundle.getString("net");
        if (SELECTED_NET.equals("FTTH")){
            if (bundle != null){
                model = (ModelFTTH) bundle.getSerializable("bundle");
                SELECTED_NET = model.getNetwork();
                SELECTED_DEVICE = model.getDevice();
                TERMINAL = model.getCheckTerminalBox();
                stNomor = model.getFATNumber();
                stSignal = model.getOutputSignalFAT();
            }else { }
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            if (bundle != null){
                modelFTTH_p = (ModelFTTH_P) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH_p.getNetwork();
                SELECTED_DEVICE = modelFTTH_p.getDevice();
                TERMINAL = modelFTTH_p.getCheckTerminalBox();
                stNomor = modelFTTH_p.getFATNumber();
                stSignal = modelFTTH_p.getOutputSignalFAT();
            }else { }
        }

        rgKonektor = v.findViewById(R.id.rg_konektor_fat);
        rbKonektorBaik = v.findViewById(R.id.rb_baik_konektor_fat);
        rbKonektorBuruk = v.findViewById(R.id.rb_buruk_konektor_fat);

        etNomor = v.findViewById(R.id.et_nomer_fat);
        etSignal = v.findViewById(R.id.et_signal_output_fat);

        texts.add(etNomor);
        texts.add(etSignal);

        data.add(stNomor);
        data.add(stSignal);

        fetchData(data);

        btnNext = v.findViewById(R.id.btn_next_fat);
        if (SELECTED_NET.equals("FTTH")){
            if (SELECTED_DEVICE.equals("ONT")){
                nextAction(R.id.ll_fragment_frame_ftth, new FragmentONT(), model, texts, SELECTED_NET);
            }else if (SELECTED_DEVICE.equals("STB 4K")){
                nextAction(R.id.ll_fragment_frame_ftth, new FragmentSTB4K(), model, texts, SELECTED_NET);
            }else if (SELECTED_DEVICE.equals("STB Lite")){
                nextAction(R.id.ll_fragment_frame_ftth, new FragmentSTBLiteFTTH(), model, texts, SELECTED_NET);
            }else { }
        }else if (SELECTED_NET.equals("FTTH-Partnership")){
            if (SELECTED_DEVICE.equals("RG")){
                nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentRG(), modelFTTH_p, texts, SELECTED_NET);
            }else if (SELECTED_DEVICE.equals("STB 4K")){
                nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentSTB4K(), modelFTTH_p, texts, SELECTED_NET);
            }else if (SELECTED_DEVICE.equals("STB Lite")){
                nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentSTBLiteFTTH(), modelFTTH_p, texts, SELECTED_NET);
            }else { }
        }else { }

        btnBack = v.findViewById(R.id.btn_back_fat);
        if (TERMINAL.equals("y")){
            if (SELECTED_NET.equals("FTTH")){
                backAction(R.id.ll_fragment_frame_ftth, new FragmentTerminalBox(), model, SELECTED_NET);
            }else if (SELECTED_NET.equals("FTTH-Partnership")){
                backAction(R.id.ll_fragment_frame_ftth_p, new FragmentTerminalBox(), modelFTTH_p, SELECTED_NET);
            }else { }
        }if (TERMINAL.equals("n")){
            if (SELECTED_NET.equals("FTTH")){
                if (SELECTED_DEVICE.equals("ONT")){
                    backAction(R.id.ll_fragment_frame_ftth, new FragmentONT(), model, SELECTED_NET);
                }else if (SELECTED_DEVICE.equals("STB 4K")){
                    backAction(R.id.ll_fragment_frame_ftth, new FragmentSTB4K(), model, SELECTED_NET);
                }else if (SELECTED_DEVICE.equals("STB Lite")){
                    backAction(R.id.ll_fragment_frame_ftth, new FragmentSTBLiteFTTH(), model, SELECTED_NET);
                }else { }
            }else if (SELECTED_NET.equals("FTTH-Partnership")){
                backAction(R.id.ll_fragment_frame_ftth_p, new FragmentONT(), modelFTTH_p, SELECTED_NET);
            }else { }
        }
    }

    private void backAction(final int from, final Fragment f, final Serializable m, final String s){
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (s.equals("FTTH")){
                    model.setState("before");
                }else if (s.equals("FTTH-Partnership")){
                    modelFTTH_p.setState("before");
                }
                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(f,"bundle", m);
                fh.goToFragment(from, f, getFragmentManager());
            }
        });
    }

    private void nextAction(final int from, final Fragment to, final Serializable m, final List<EditText> txt, final String s){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int konektor = rgKonektor.getCheckedRadioButtonId();
                if (    txt.get(0).getText().toString().isEmpty() ||
                        txt.get(1).getText().toString().isEmpty() ){
                    ValidateHelper vh = new ValidateHelper();
                    vh.validasi(txt);
                }else if (konektor <= 0){
                    rgKonektor.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Konektor belum dicheck", Toast.LENGTH_SHORT).show();
                }else {
                    if (konektor == rbKonektorBaik.getId()){
                        stkonektor = rbKonektorBaik.getText().toString();
                    }else if (konektor == rbKonektorBuruk.getId()){
                        stkonektor = rbKonektorBuruk.getText().toString(); }

                    if (s.equals("FTTH")){
                        model.setState("after");
                        model.setConnectorPatchCordFAT(stkonektor);
                        model.setFATNumber(txt.get(0).getText().toString());
                        model.setOutputSignalFAT(txt.get(1).getText().toString());
                    }else if (s.equals("FTTH-Partnership")){
                        modelFTTH_p.setState("after");
                        modelFTTH_p.setConnectorPatchCordFAT(stkonektor);
                        modelFTTH_p.setFATNumber(txt.get(0).getText().toString());
                        modelFTTH_p.setOutputSignalFAT(txt.get(1).getText().toString());
                    }
                    FragmentHelper fh = new FragmentHelper();
                    fh.NET = s;
                    fh.extra(to, "bundle", m);
                    fh.goToFragment(from, to, getFragmentManager());
                }
            }
        });
    }

    private void fetchData(List<String> str){
        for (int i = 0; i < str.size(); i++) {
            if (!str.isEmpty()){
                etNomor.setText(str.get(0));
                etSignal.setText(str.get(1)); } else { }
        }
    }
}
