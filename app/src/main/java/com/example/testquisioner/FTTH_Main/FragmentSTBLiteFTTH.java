package com.example.testquisioner.FTTH_Main;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.testquisioner.Helper.DialogHelper;
import com.example.testquisioner.FTTH_Main.FTTH.MainActivityFTTH;
import com.example.testquisioner.FTTH_Main.FTTH_Partnership.MainActivityFTTH_P;
import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.General.FragmentRegisterModem;
import com.example.testquisioner.Model.ModelFTTH;
import com.example.testquisioner.Model.ModelFTTH_P;
import com.example.testquisioner.R;
import com.example.testquisioner.Helper.ValidateHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSTBLiteFTTH extends Fragment {
    private Button btnNext, btnBack;
    private ImageButton btnTambah, btnKurang;
    private LinearLayout channel1, channel2;
    private EditText etNumChannel1, etPacket1, etJitter1, etIp1;
    private EditText etNumChannel2, etPacket2, etJitter2, etIp2;
    private EditText etNumChannel3, etPacket3, etJitter3, etIp3;

    private ModelFTTH model;
    private ModelFTTH_P modelFTTH_p;

    private String SELECTED_NET = "";
    private String STATE = "";
    private int CHANNEL = 0;

    private List<EditText> texts = new ArrayList<>();

    public FragmentSTBLiteFTTH() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stblite_ftth, container, false);
        getActivity().setTitle("Check STB Lite");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        SELECTED_NET = bundle.getString("net");

        if (bundle != null) {
            if (SELECTED_NET.equals("FTTH")){
                model = (ModelFTTH) bundle.getSerializable("bundle");
                SELECTED_NET = model.getNetwork();
                STATE = model.getState();
                }else if (SELECTED_NET.equals("FTTH-Partnership")){
                modelFTTH_p = (ModelFTTH_P) bundle.getSerializable("bundle");
                SELECTED_NET = modelFTTH_p.getNetwork();
                STATE = modelFTTH_p.getState();
                }
        } else { }

        etNumChannel1 = v.findViewById(R.id.et_no_channel_stblite_ftth);
        etPacket1 = v.findViewById(R.id.et_packet_drop_stblite_ftth);
        etJitter1 = v.findViewById(R.id.et_jitter_stblite_ftth);
        etIp1 = v.findViewById(R.id.et_ip_stblite_ftth);

        texts.add(etNumChannel1);
        texts.add(etPacket1);
        texts.add(etJitter1);
        texts.add(etIp1);

        etNumChannel2 = v.findViewById(R.id.et_no_channel_stblite_ftth_1);
        etPacket2 = v.findViewById(R.id.et_packet_drop_stblite_ftth_1);
        etJitter2 = v.findViewById(R.id.et_jitter_stblite_ftth_1);
        etIp2 = v.findViewById(R.id.et_ip_stblite_ftth_1);

        texts.add(etNumChannel2);
        texts.add(etPacket2);
        texts.add(etJitter2);
        texts.add(etIp2);

        etNumChannel3 = v.findViewById(R.id.et_no_channel_stblite_ftth_2);
        etPacket3 = v.findViewById(R.id.et_packet_drop_stblite_ftth_2);
        etJitter3 = v.findViewById(R.id.et_jitter_stblite_ftth_2);
        etIp3 = v.findViewById(R.id.et_ip_stblite_ftth_2);

        texts.add(etNumChannel3);
        texts.add(etPacket3);
        texts.add(etJitter3);
        texts.add(etIp3);

        channel1 = v.findViewById(R.id.ll_check_stblite_ftth_1);
        channel2 = v.findViewById(R.id.ll_check_stblite_ftth_2);

        btnNext = v.findViewById(R.id.btn_next_stblite_ftth);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CHANNEL == 0 ){
                    if (    texts.get(0).getText().toString().isEmpty() ||
                            texts.get(1).getText().toString().isEmpty() ||
                            texts.get(2).getText().toString().isEmpty() ||
                            texts.get(3).getText().toString().isEmpty() ){
                        ValidateHelper vh = new ValidateHelper();
                        vh.validasi(texts);
                    }else {
                        //go
                        nextStep(SELECTED_NET);
                    }
                }else if (CHANNEL == 1){
                    if (    texts.get(0).getText().toString().isEmpty() ||
                            texts.get(1).getText().toString().isEmpty() ||
                            texts.get(2).getText().toString().isEmpty() ||
                            texts.get(3).getText().toString().isEmpty() ||
                            texts.get(4).getText().toString().isEmpty() ||
                            texts.get(5).getText().toString().isEmpty() ||
                            texts.get(6).getText().toString().isEmpty() ||
                            texts.get(7).getText().toString().isEmpty() ){
                        ValidateHelper vh = new ValidateHelper();
                        vh.validasi(texts);
                    }else {
                        //go
                        nextStep(SELECTED_NET);
                    }
                }else if (CHANNEL == 2){
                    if (    texts.get(0).getText().toString().isEmpty() ||
                            texts.get(1).getText().toString().isEmpty() ||
                            texts.get(2).getText().toString().isEmpty() ||
                            texts.get(3).getText().toString().isEmpty() ||
                            texts.get(4).getText().toString().isEmpty() ||
                            texts.get(5).getText().toString().isEmpty() ||
                            texts.get(6).getText().toString().isEmpty() ||
                            texts.get(7).getText().toString().isEmpty() ||
                            texts.get(8).getText().toString().isEmpty() ||
                            texts.get(9).getText().toString().isEmpty() ||
                            texts.get(10).getText().toString().isEmpty() ||
                            texts.get(11).getText().toString().isEmpty() ){
                        ValidateHelper vh = new ValidateHelper();
                        vh.validasi(texts);
                    }else {
                        //go
                        nextStep(SELECTED_NET);
                    }
                }
            }
        });

        btnBack = v.findViewById(R.id.btn_back_stblite_ftth);
        if (STATE.equals("before")){
            if (SELECTED_NET.equals("FTTH")) {
                backAction(MainActivityFTTH.class, model);
            } else if (SELECTED_NET.equals("FTTH-Partnership")) {
                backAction(MainActivityFTTH_P.class, modelFTTH_p);
            } else { }
        }else if (STATE.equals("after")){
            if (SELECTED_NET.equals("FTTH")){
                backFragment(R.id.ll_fragment_frame_ftth, new FragmentFAT(), model, SELECTED_NET);
            }else if (SELECTED_NET.equals("FTTH-Partnership")){
                backFragment(R.id.ll_fragment_frame_ftth_p, new FragmentFAT(), modelFTTH_p, SELECTED_NET);
            }else { }
        }else { }

        btnTambah = v.findViewById(R.id.btn_tambah_channel__stblite_ftth);
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CHANNEL == 0){
                    channel1.setVisibility(View.VISIBLE);
                    channel1.setAlpha(0.0f);
                    channel1.animate().alpha(1.0f).setDuration(1000);
                    btnKurang.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(), "Check channel 2 telah ditambahkan", Toast.LENGTH_SHORT).show();
                    CHANNEL ++;
                }else if (CHANNEL == 1){
                    channel2.setVisibility(View.VISIBLE);
                    channel2.setAlpha(0.0f);
                    channel2.animate().alpha(1.0f).setDuration(1000);
                    Toast.makeText(getContext(), "Check channel 3 telah ditambahkan", Toast.LENGTH_SHORT).show();
                    CHANNEL++;
                }else { }
            }
        });

        btnKurang = v.findViewById(R.id.btn_kurang_channel__stblite_ftth);
        btnKurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CHANNEL == 1){
                    channel2.setAlpha(1.0f);
                    channel2.animate().alpha(0.0f).setDuration(2000);
                    channel2.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Check channel 3 telah dihapus", Toast.LENGTH_SHORT).show();
                    CHANNEL --;
                }else if (CHANNEL == 0){
                    channel1.setAlpha(1.0f);
                    channel1.animate().alpha(0.0f).setDuration(2000);
                    channel1.setVisibility(View.GONE);
                    btnKurang.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Check channel 2 telah dihapus", Toast.LENGTH_SHORT).show();
                    CHANNEL--;
                }else { }
            }
        });
    }

    private void startDialog(final ModelFTTH m, final String s) {
        DialogHelper dh = new DialogHelper();
        dh.setupDialog(new AlertDialog.Builder(getContext()), "Apakah Terminal Box bisa diakses ?",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //positive
                        m.setCheckTerminalBox("y");
                        nextAction(R.id.ll_fragment_frame_ftth, new FragmentTerminalBox(), m, s);
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //negative
                        //go to FAT
                        m.setCheckTerminalBox("n");
                        nextAction(R.id.ll_fragment_frame_ftth, new FragmentFAT(), m, s);
                    }
                });
    }

    private void backAction(final Class<?> c, final Serializable m){
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),c);
                intent.putExtra("bundle", m);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    private void backFragment(final int from, final Fragment to, final Serializable m, final String s){
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.NET = s;
                fh.extra(to, "bundle", m);
                fh.goToFragment(from, to, getFragmentManager());
            }
        });
    }

    private void nextAction(final int from, final Fragment to, final Serializable m, String s){
        FragmentHelper fh = new FragmentHelper();
        fh.NET = s;
        fh.extra(to, "bundle", m);
        fh.goToFragment(from, to, getFragmentManager());
    }

    private void sendToModelFTTH(ModelFTTH m, List<EditText> edt){
        m.setChannelSTB1(edt.get(0).getText().toString());
        m.setPacketDropSTB1(edt.get(1).getText().toString());
        m.setJitterSTB1(edt.get(2).getText().toString());
        m.setIpAddressSTB1(edt.get(3).getText().toString());
        m.setChannelSTB2(edt.get(4).getText().toString());
        m.setPacketDropSTB2(edt.get(5).getText().toString());
        m.setJitterSTB2(edt.get(6).getText().toString());
        m.setIpAddressSTB2(edt.get(7).getText().toString());
        m.setChannelSTB3(edt.get(8).getText().toString());
        m.setPacketDropSTB3(edt.get(9).getText().toString());
        m.setJitterSTB3(edt.get(10).getText().toString());
        m.setIpAddressSTB3(edt.get(11).getText().toString());
    }

    private void sendToModelFTTH_p(ModelFTTH_P m, List<EditText> edt){
        m.setChannelSTB1(edt.get(0).getText().toString());
        m.setPacketDropSTB1(edt.get(1).getText().toString());
        m.setJitterSTB1(edt.get(2).getText().toString());
        m.setIpAddressSTB1(edt.get(3).getText().toString());
        m.setChannelSTB2(edt.get(4).getText().toString());
        m.setPacketDropSTB2(edt.get(5).getText().toString());
        m.setJitterSTB2(edt.get(6).getText().toString());
        m.setIpAddressSTB2(edt.get(7).getText().toString());
        m.setChannelSTB3(edt.get(8).getText().toString());
        m.setPacketDropSTB3(edt.get(9).getText().toString());
        m.setJitterSTB3(edt.get(10).getText().toString());
        m.setIpAddressSTB3(edt.get(11).getText().toString());
    }

    private void nextStep(String net){
        if (STATE.equals("before")){
            if (SELECTED_NET.equals("FTTH")){
                sendToModelFTTH(model, texts);
                startDialog(model, net);
            }else if (SELECTED_NET.equals("FTTH-Partnership")){
                sendToModelFTTH_p(modelFTTH_p, texts);
                nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentONT(), modelFTTH_p, net);
            }else { }
        }else if (STATE.equals("after")){
            if (SELECTED_NET.equals("FTTH")){
                sendToModelFTTH(model, texts);
                nextAction(R.id.ll_fragment_frame_ftth, new FragmentRegisterModem(), model, net);
            }else if (SELECTED_NET.equals("FTTH-Partnership")){
                sendToModelFTTH_p(modelFTTH_p, texts);
                nextAction(R.id.ll_fragment_frame_ftth_p, new FragmentRegisterModem(), modelFTTH_p, net);
            }else { }
        }
    }
}
