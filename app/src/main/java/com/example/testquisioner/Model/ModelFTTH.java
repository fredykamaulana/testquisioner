package com.example.testquisioner.Model;

import java.io.Serializable;

public class ModelFTTH implements Serializable {
    private String network;
    private String device;
    private String type;
    private String state;
    private String spliter;
    private String ground;
    private String terminalBox;
    private String nf;
    private String patchCordONT;
    private String adapterPatchCordONT;
    private String outputSignalONT;
    private String typeSTB;
    private String channelSTB1;
    private String packetDropSTB1;
    private String jitterSTB1;
    private String ipAddressSTB1;
    private String channelSTB2;
    private String packetDropSTB2;
    private String jitterSTB2;
    private String ipAddressSTB2;
    private String channelSTB3;
    private String packetDropSTB3;
    private String jitterSTB3;
    private String ipAddressSTB3;
    private String youtubeHooqSTB;
    private String checkTerminalBox;
    private String dropWireBox;
    private String splicingBox;
    private String patchCordBox;
    private String outputSignalBox;
    private String FATNumber;
    private String connectorPatchCordFAT;
    private String outputSignalFAT;
    private String merkTypeRouter;
    private String SerialNumberRouter;
    private String powerSignallRouter;
    private String band24ChannelRouter;
    private String band5CahnnelRouter;
    private String coverageRouter;
    private String LinkSpeedRouter;

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSpliter() {
        return spliter;
    }

    public void setSpliter(String spliter) {
        this.spliter = spliter;
    }

    public String getGround() {
        return ground;
    }

    public void setGround(String ground) {
        this.ground = ground;
    }

    public String getTerminalBox() {
        return terminalBox;
    }

    public void setTerminalBox(String terminalBox) {
        this.terminalBox = terminalBox;
    }

    public String getNf() {
        return nf;
    }

    public void setNf(String nf) {
        this.nf = nf;
    }

    public String getPatchCordONT() {
        return patchCordONT;
    }

    public void setPatchCordONT(String patchCordONT) {
        this.patchCordONT = patchCordONT;
    }

    public String getAdapterPatchCordONT() {
        return adapterPatchCordONT;
    }

    public void setAdapterPatchCordONT(String adapterPatchCordONT) {
        this.adapterPatchCordONT = adapterPatchCordONT;
    }

    public String getOutputSignalONT() {
        return outputSignalONT;
    }

    public void setOutputSignalONT(String outputSignalONT) {
        this.outputSignalONT = outputSignalONT;
    }

    public String getTypeSTB() {
        return typeSTB;
    }

    public void setTypeSTB(String typeSTB) {
        this.typeSTB = typeSTB;
    }

    public String getChannelSTB1() {
        return channelSTB1;
    }

    public void setChannelSTB1(String channelSTB1) {
        this.channelSTB1 = channelSTB1;
    }

    public String getPacketDropSTB1() {
        return packetDropSTB1;
    }

    public void setPacketDropSTB1(String packetDropSTB1) {
        this.packetDropSTB1 = packetDropSTB1;
    }

    public String getJitterSTB1() {
        return jitterSTB1;
    }

    public void setJitterSTB1(String jitterSTB1) {
        this.jitterSTB1 = jitterSTB1;
    }

    public String getIpAddressSTB1() {
        return ipAddressSTB1;
    }

    public void setIpAddressSTB1(String ipAddressSTB1) {
        this.ipAddressSTB1 = ipAddressSTB1;
    }

    public String getChannelSTB2() {
        return channelSTB2;
    }

    public void setChannelSTB2(String channelSTB2) {
        this.channelSTB2 = channelSTB2;
    }

    public String getPacketDropSTB2() {
        return packetDropSTB2;
    }

    public void setPacketDropSTB2(String packetDropSTB2) {
        this.packetDropSTB2 = packetDropSTB2;
    }

    public String getJitterSTB2() {
        return jitterSTB2;
    }

    public void setJitterSTB2(String jitterSTB2) {
        this.jitterSTB2 = jitterSTB2;
    }

    public String getIpAddressSTB2() {
        return ipAddressSTB2;
    }

    public void setIpAddressSTB2(String ipAddressSTB2) {
        this.ipAddressSTB2 = ipAddressSTB2;
    }

    public String getChannelSTB3() {
        return channelSTB3;
    }

    public void setChannelSTB3(String channelSTB3) {
        this.channelSTB3 = channelSTB3;
    }

    public String getPacketDropSTB3() {
        return packetDropSTB3;
    }

    public void setPacketDropSTB3(String packetDropSTB3) {
        this.packetDropSTB3 = packetDropSTB3;
    }

    public String getJitterSTB3() {
        return jitterSTB3;
    }

    public void setJitterSTB3(String jitterSTB3) {
        this.jitterSTB3 = jitterSTB3;
    }

    public String getIpAddressSTB3() {
        return ipAddressSTB3;
    }

    public void setIpAddressSTB3(String ipAddressSTB3) {
        this.ipAddressSTB3 = ipAddressSTB3;
    }

    public String getYoutubeHooqSTB() {
        return youtubeHooqSTB;
    }

    public void setYoutubeHooqSTB(String youtubeHooqSTB) {
        this.youtubeHooqSTB = youtubeHooqSTB;
    }

    public String getCheckTerminalBox() {
        return checkTerminalBox;
    }

    public void setCheckTerminalBox(String checkTerminalBox) {
        this.checkTerminalBox = checkTerminalBox;
    }

    public String getDropWireBox() {
        return dropWireBox;
    }

    public void setDropWireBox(String dropWireBox) {
        this.dropWireBox = dropWireBox;
    }

    public String getSplicingBox() {
        return splicingBox;
    }

    public void setSplicingBox(String splicingBox) {
        this.splicingBox = splicingBox;
    }

    public String getPatchCordBox() {
        return patchCordBox;
    }

    public void setPatchCordBox(String patchCordBox) {
        this.patchCordBox = patchCordBox;
    }

    public String getOutputSignalBox() {
        return outputSignalBox;
    }

    public void setOutputSignalBox(String outputSignalBox) {
        this.outputSignalBox = outputSignalBox;
    }

    public String getFATNumber() {
        return FATNumber;
    }

    public void setFATNumber(String FATNumber) {
        this.FATNumber = FATNumber;
    }

    public String getConnectorPatchCordFAT() {
        return connectorPatchCordFAT;
    }

    public void setConnectorPatchCordFAT(String connectorPatchCordFAT) {
        this.connectorPatchCordFAT = connectorPatchCordFAT;
    }

    public String getOutputSignalFAT() {
        return outputSignalFAT;
    }

    public void setOutputSignalFAT(String outputSignalFAT) {
        this.outputSignalFAT = outputSignalFAT;
    }

    public String getMerkTypeRouter() {
        return merkTypeRouter;
    }

    public void setMerkTypeRouter(String merkTypeRouter) {
        this.merkTypeRouter = merkTypeRouter;
    }

    public String getSerialNumberRouter() {
        return SerialNumberRouter;
    }

    public void setSerialNumberRouter(String serialNumberRouter) {
        SerialNumberRouter = serialNumberRouter;
    }

    public String getPowerSignallRouter() {
        return powerSignallRouter;
    }

    public void setPowerSignallRouter(String powerSignallRouter) {
        this.powerSignallRouter = powerSignallRouter;
    }

    public String getBand24ChannelRouter() {
        return band24ChannelRouter;
    }

    public void setBand24ChannelRouter(String band24ChannelRouter) {
        this.band24ChannelRouter = band24ChannelRouter;
    }

    public String getBand5CahnnelRouter() {
        return band5CahnnelRouter;
    }

    public void setBand5CahnnelRouter(String band5CahnnelRouter) {
        this.band5CahnnelRouter = band5CahnnelRouter;
    }

    public String getCoverageRouter() {
        return coverageRouter;
    }

    public void setCoverageRouter(String coverageRouter) {
        this.coverageRouter = coverageRouter;
    }

    public String getLinkSpeedRouter() {
        return LinkSpeedRouter;
    }

    public void setLinkSpeedRouter(String linkSpeedRouter) {
        LinkSpeedRouter = linkSpeedRouter;
    }
}
