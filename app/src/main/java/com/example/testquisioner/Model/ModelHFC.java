package com.example.testquisioner.Model;

import java.io.Serializable;

public class ModelHFC implements Serializable {
    private String network;
    private String device;
    private String type;
    private String state;
    private String spliter;
    private String ground;
    private String terminalBox;
    private String nf;
    private String typeModem;
    private String boundingChannelDownstreamModem;
    private String boundingChannelUpstreamModem;
    private String boundingChannelSNRModem;
    private String signalDownstreamModem;
    private String signalUpstreamModem;
    private String signalSNRModem;
    private String wirelessBand24ChannelModem;
    private String wirelessBand5ChannelModem;
    private String wirelessCoverageModem;
    private String typeSTB;
    private String channelSTB1;
    private String strengthPowerSTB1;
    private String strengthQualitySTB1;
    private String cnSTB1;
    private String berSTB1;
    private String downStreamX1STB1;
    private String upstreamX1STB1;
    private String snrX1STB1;
    private String channelSTB2;
    private String strengthPowerSTB2;
    private String strengthQualitySTB2;
    private String cnSTB2;
    private String berSTB2;
    private String downStreamX1STB2;
    private String upstreamX1STB2;
    private String snrX1STB2;
    private String channelSTB3;
    private String strengthPowerSTB3;
    private String strengthQualitySTB3;
    private String cnSTB3;
    private String berSTB3;
    private String downStreamX1STB3;
    private String upstreamX1STB3;
    private String snrX1STB3;
    private String youtubeHooqX1STB;
    private String coaxOutlet;
    private String wallplateOutlet;
    private String conecctorInputtoModem;
    private String jumperOutlet;
    private String checkSplitter;
    private String coaxSplitter;
    private String conditionSplitter;
    private String connectorInnSplitter;
    private String connectorOutSplitter;
    private String signalDownstreamSplitter;
    private String signalUpstreamSplitter;
    private String signalSNRSplitter;
    private String checkTestPoint;
    private String cableDropTestPoint;
    private String conditionTestPoint;
    private String connectorInnPoint;
    private String connectorOutPoint;
    private String signalDownstreamPoint;
    private String signalUpstreamPoint;
    private String signalSNRPoint;
    private String conditionFConnectorTap;
    private String signalDownstreamTap;
    private String signalUpstreamTap;
    private String signalSNRTap;
    private String TapFull;
    private String TapValue;
    private String cableDropToTap;
    private String merkTypeRouter;
    private String SerialNumberRouter;
    private String powerSignallRouter;
    private String band24ChannelRouter;
    private String band5ChannelRouter;
    private String coverageRouter;
    private String LinkSpeedRouter;

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSpliter() {
        return spliter;
    }

    public void setSpliter(String spliter) {
        this.spliter = spliter;
    }

    public String getGround() {
        return ground;
    }

    public void setGround(String ground) {
        this.ground = ground;
    }

    public String getTerminalBox() {
        return terminalBox;
    }

    public void setTerminalBox(String terminalBox) {
        this.terminalBox = terminalBox;
    }

    public String getNf() {
        return nf;
    }

    public void setNf(String nf) {
        this.nf = nf;
    }

    public String getTypeModem() {
        return typeModem;
    }

    public void setTypeModem(String typeModem) {
        this.typeModem = typeModem;
    }

    public String getBoundingChannelDownstreamModem() {
        return boundingChannelDownstreamModem;
    }

    public void setBoundingChannelDownstreamModem(String boundingChannelDownstreamModem) {
        this.boundingChannelDownstreamModem = boundingChannelDownstreamModem;
    }

    public String getBoundingChannelUpstreamModem() {
        return boundingChannelUpstreamModem;
    }

    public void setBoundingChannelUpstreamModem(String boundingChannelUpstreamModem) {
        this.boundingChannelUpstreamModem = boundingChannelUpstreamModem;
    }

    public String getBoundingChannelSNRModem() {
        return boundingChannelSNRModem;
    }

    public void setBoundingChannelSNRModem(String boundingChannelSNRModem) {
        this.boundingChannelSNRModem = boundingChannelSNRModem;
    }

    public String getSignalDownstreamModem() {
        return signalDownstreamModem;
    }

    public void setSignalDownstreamModem(String signalDownstreamModem) {
        this.signalDownstreamModem = signalDownstreamModem;
    }

    public String getSignalUpstreamModem() {
        return signalUpstreamModem;
    }

    public void setSignalUpstreamModem(String signalUpstreamModem) {
        this.signalUpstreamModem = signalUpstreamModem;
    }

    public String getSignalSNRModem() {
        return signalSNRModem;
    }

    public void setSignalSNRModem(String signalSNRModem) {
        this.signalSNRModem = signalSNRModem;
    }

    public String getWirelessBand24ChannelModem() {
        return wirelessBand24ChannelModem;
    }

    public void setWirelessBand24ChannelModem(String wirelessBand24ChannelModem) {
        this.wirelessBand24ChannelModem = wirelessBand24ChannelModem;
    }

    public String getWirelessBand5ChannelModem() {
        return wirelessBand5ChannelModem;
    }

    public void setWirelessBand5ChannelModem(String wirelessBand5ChannelModem) {
        this.wirelessBand5ChannelModem = wirelessBand5ChannelModem;
    }

    public String getWirelessCoverageModem() {
        return wirelessCoverageModem;
    }

    public void setWirelessCoverageModem(String wirelessCoverageModem) {
        this.wirelessCoverageModem = wirelessCoverageModem;
    }

    public String getTypeSTB() {
        return typeSTB;
    }

    public void setTypeSTB(String typeSTB) {
        this.typeSTB = typeSTB;
    }

    public String getChannelSTB1() {
        return channelSTB1;
    }

    public void setChannelSTB1(String channelSTB1) {
        this.channelSTB1 = channelSTB1;
    }

    public String getStrengthPowerSTB1() {
        return strengthPowerSTB1;
    }

    public void setStrengthPowerSTB1(String strengthPowerSTB1) {
        this.strengthPowerSTB1 = strengthPowerSTB1;
    }

    public String getStrengthQualitySTB1() {
        return strengthQualitySTB1;
    }

    public void setStrengthQualitySTB1(String strengthQualitySTB1) {
        this.strengthQualitySTB1 = strengthQualitySTB1;
    }

    public String getCnSTB1() {
        return cnSTB1;
    }

    public void setCnSTB1(String cnSTB1) {
        this.cnSTB1 = cnSTB1;
    }

    public String getBerSTB1() {
        return berSTB1;
    }

    public void setBerSTB1(String berSTB1) {
        this.berSTB1 = berSTB1;
    }

    public String getDownStreamX1STB1() {
        return downStreamX1STB1;
    }

    public void setDownStreamX1STB1(String downStreamX1STB1) {
        this.downStreamX1STB1 = downStreamX1STB1;
    }

    public String getUpstreamX1STB1() {
        return upstreamX1STB1;
    }

    public void setUpstreamX1STB1(String upstreamX1STB1) {
        this.upstreamX1STB1 = upstreamX1STB1;
    }

    public String getSnrX1STB1() {
        return snrX1STB1;
    }

    public void setSnrX1STB1(String snrX1STB1) {
        this.snrX1STB1 = snrX1STB1;
    }

    public String getChannelSTB2() {
        return channelSTB2;
    }

    public void setChannelSTB2(String channelSTB2) {
        this.channelSTB2 = channelSTB2;
    }

    public String getStrengthPowerSTB2() {
        return strengthPowerSTB2;
    }

    public void setStrengthPowerSTB2(String strengthPowerSTB2) {
        this.strengthPowerSTB2 = strengthPowerSTB2;
    }

    public String getStrengthQualitySTB2() {
        return strengthQualitySTB2;
    }

    public void setStrengthQualitySTB2(String strengthQualitySTB2) {
        this.strengthQualitySTB2 = strengthQualitySTB2;
    }

    public String getCnSTB2() {
        return cnSTB2;
    }

    public void setCnSTB2(String cnSTB2) {
        this.cnSTB2 = cnSTB2;
    }

    public String getBerSTB2() {
        return berSTB2;
    }

    public void setBerSTB2(String berSTB2) {
        this.berSTB2 = berSTB2;
    }

    public String getDownStreamX1STB2() {
        return downStreamX1STB2;
    }

    public void setDownStreamX1STB2(String downStreamX1STB2) {
        this.downStreamX1STB2 = downStreamX1STB2;
    }

    public String getUpstreamX1STB2() {
        return upstreamX1STB2;
    }

    public void setUpstreamX1STB2(String upstreamX1STB2) {
        this.upstreamX1STB2 = upstreamX1STB2;
    }

    public String getSnrX1STB2() {
        return snrX1STB2;
    }

    public void setSnrX1STB2(String snrX1STB2) {
        this.snrX1STB2 = snrX1STB2;
    }

    public String getChannelSTB3() {
        return channelSTB3;
    }

    public void setChannelSTB3(String channelSTB3) {
        this.channelSTB3 = channelSTB3;
    }

    public String getStrengthPowerSTB3() {
        return strengthPowerSTB3;
    }

    public void setStrengthPowerSTB3(String strengthPowerSTB3) {
        this.strengthPowerSTB3 = strengthPowerSTB3;
    }

    public String getStrengthQualitySTB3() {
        return strengthQualitySTB3;
    }

    public void setStrengthQualitySTB3(String strengthQualitySTB3) {
        this.strengthQualitySTB3 = strengthQualitySTB3;
    }

    public String getCnSTB3() {
        return cnSTB3;
    }

    public void setCnSTB3(String cnSTB3) {
        this.cnSTB3 = cnSTB3;
    }

    public String getBerSTB3() {
        return berSTB3;
    }

    public void setBerSTB3(String berSTB3) {
        this.berSTB3 = berSTB3;
    }

    public String getDownStreamX1STB3() {
        return downStreamX1STB3;
    }

    public void setDownStreamX1STB3(String downStreamX1STB3) {
        this.downStreamX1STB3 = downStreamX1STB3;
    }

    public String getUpstreamX1STB3() {
        return upstreamX1STB3;
    }

    public void setUpstreamX1STB3(String upstreamX1STB3) {
        this.upstreamX1STB3 = upstreamX1STB3;
    }

    public String getSnrX1STB3() {
        return snrX1STB3;
    }

    public void setSnrX1STB3(String snrX1STB3) {
        this.snrX1STB3 = snrX1STB3;
    }

    public String getYoutubeHooqX1STB() {
        return youtubeHooqX1STB;
    }

    public void setYoutubeHooqX1STB(String youtubeHooqX1STB) {
        this.youtubeHooqX1STB = youtubeHooqX1STB;
    }

    public String getCoaxOutlet() {
        return coaxOutlet;
    }

    public void setCoaxOutlet(String coaxOutlet) {
        this.coaxOutlet = coaxOutlet;
    }

    public String getWallplateOutlet() {
        return wallplateOutlet;
    }

    public void setWallplateOutlet(String wallplateOutlet) {
        this.wallplateOutlet = wallplateOutlet;
    }

    public String getConecctorInputtoModem() {
        return conecctorInputtoModem;
    }

    public void setConecctorInputtoModem(String conecctorInputtoModem) {
        this.conecctorInputtoModem = conecctorInputtoModem;
    }

    public String getJumperOutlet() {
        return jumperOutlet;
    }

    public void setJumperOutlet(String jumperOutlet) {
        this.jumperOutlet = jumperOutlet;
    }

    public String getCheckSplitter() {
        return checkSplitter;
    }

    public void setCheckSplitter(String checkSplitter) {
        this.checkSplitter = checkSplitter;
    }

    public String getCoaxSplitter() {
        return coaxSplitter;
    }

    public void setCoaxSplitter(String coaxSplitter) {
        this.coaxSplitter = coaxSplitter;
    }

    public String getConditionSplitter() {
        return conditionSplitter;
    }

    public void setConditionSplitter(String conditionSplitter) {
        this.conditionSplitter = conditionSplitter;
    }

    public String getConnectorInnSplitter() {
        return connectorInnSplitter;
    }

    public void setConnectorInnSplitter(String connectorInnSplitter) {
        this.connectorInnSplitter = connectorInnSplitter;
    }

    public String getConnectorOutSplitter() {
        return connectorOutSplitter;
    }

    public void setConnectorOutSplitter(String connectorOutSplitter) {
        this.connectorOutSplitter = connectorOutSplitter;
    }

    public String getSignalDownstreamSplitter() {
        return signalDownstreamSplitter;
    }

    public void setSignalDownstreamSplitter(String signalDownstreamSplitter) {
        this.signalDownstreamSplitter = signalDownstreamSplitter;
    }

    public String getSignalUpstreamSplitter() {
        return signalUpstreamSplitter;
    }

    public void setSignalUpstreamSplitter(String signalUpstreamSplitter) {
        this.signalUpstreamSplitter = signalUpstreamSplitter;
    }

    public String getSignalSNRSplitter() {
        return signalSNRSplitter;
    }

    public void setSignalSNRSplitter(String signalSNRSplitter) {
        this.signalSNRSplitter = signalSNRSplitter;
    }

    public String getCheckTestPoint() {
        return checkTestPoint;
    }

    public void setCheckTestPoint(String checkTestPoint) {
        this.checkTestPoint = checkTestPoint;
    }

    public String getCableDropTestPoint() {
        return cableDropTestPoint;
    }

    public void setCableDropTestPoint(String cableDropTestPoint) {
        this.cableDropTestPoint = cableDropTestPoint;
    }

    public String getConditionTestPoint() {
        return conditionTestPoint;
    }

    public void setConditionTestPoint(String conditionTestPoint) {
        this.conditionTestPoint = conditionTestPoint;
    }

    public String getConnectorInnPoint() {
        return connectorInnPoint;
    }

    public void setConnectorInnPoint(String connectorInnPoint) {
        this.connectorInnPoint = connectorInnPoint;
    }

    public String getConnectorOutPoint() {
        return connectorOutPoint;
    }

    public void setConnectorOutPoint(String connectorOutPoint) {
        this.connectorOutPoint = connectorOutPoint;
    }

    public String getSignalDownstreamPoint() {
        return signalDownstreamPoint;
    }

    public void setSignalDownstreamPoint(String signalDownstreamPoint) {
        this.signalDownstreamPoint = signalDownstreamPoint;
    }

    public String getSignalUpstreamPoint() {
        return signalUpstreamPoint;
    }

    public void setSignalUpstreamPoint(String signalUpstreamPoint) {
        this.signalUpstreamPoint = signalUpstreamPoint;
    }

    public String getSignalSNRPoint() {
        return signalSNRPoint;
    }

    public void setSignalSNRPoint(String signalSNRPoint) {
        this.signalSNRPoint = signalSNRPoint;
    }

    public String getConditionFConnectorTap() {
        return conditionFConnectorTap;
    }

    public void setConditionFConnectorTap(String conditionFConnectorTap) {
        this.conditionFConnectorTap = conditionFConnectorTap;
    }

    public String getSignalDownstreamTap() {
        return signalDownstreamTap;
    }

    public void setSignalDownstreamTap(String signalDownstreamTap) {
        this.signalDownstreamTap = signalDownstreamTap;
    }

    public String getSignalUpstreamTap() {
        return signalUpstreamTap;
    }

    public void setSignalUpstreamTap(String signalUpstreamTap) {
        this.signalUpstreamTap = signalUpstreamTap;
    }

    public String getSignalSNRTap() {
        return signalSNRTap;
    }

    public void setSignalSNRTap(String signalSNRTap) {
        this.signalSNRTap = signalSNRTap;
    }

    public String getTapFull() {
        return TapFull;
    }

    public void setTapFull(String tapFull) {
        TapFull = tapFull;
    }

    public String getTapValue() {
        return TapValue;
    }

    public void setTapValue(String tapValue) {
        TapValue = tapValue;
    }

    public String getCableDropToTap() {
        return cableDropToTap;
    }

    public void setCableDropToTap(String cableDropToTap) {
        this.cableDropToTap = cableDropToTap;
    }

    public String getMerkTypeRouter() {
        return merkTypeRouter;
    }

    public void setMerkTypeRouter(String merkTypeRouter) {
        this.merkTypeRouter = merkTypeRouter;
    }

    public String getSerialNumberRouter() {
        return SerialNumberRouter;
    }

    public void setSerialNumberRouter(String serialNumberRouter) {
        SerialNumberRouter = serialNumberRouter;
    }

    public String getPowerSignallRouter() {
        return powerSignallRouter;
    }

    public void setPowerSignallRouter(String powerSignallRouter) {
        this.powerSignallRouter = powerSignallRouter;
    }

    public String getBand24ChannelRouter() {
        return band24ChannelRouter;
    }

    public void setBand24ChannelRouter(String band24ChannelRouter) {
        this.band24ChannelRouter = band24ChannelRouter;
    }

    public String getBand5ChannelRouter() {
        return band5ChannelRouter;
    }

    public void setBand5ChannelRouter(String band5ChannelRouter) {
        this.band5ChannelRouter = band5ChannelRouter;
    }

    public String getCoverageRouter() {
        return coverageRouter;
    }

    public void setCoverageRouter(String coverageRouter) {
        this.coverageRouter = coverageRouter;
    }

    public String getLinkSpeedRouter() {
        return LinkSpeedRouter;
    }

    public void setLinkSpeedRouter(String linkSpeedRouter) {
        LinkSpeedRouter = linkSpeedRouter;
    }
}
