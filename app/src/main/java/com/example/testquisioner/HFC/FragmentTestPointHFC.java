package com.example.testquisioner.HFC;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;
import com.example.testquisioner.Helper.ValidateHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTestPointHFC extends Fragment {
    private Button btnNext, btnBack;
    private RadioGroup rgDrop, rgTestP, rgInput, rgOutput;
    private RadioButton rbDropBaik, rbDropBuruk, rbTestPBaik, rbTestPBuruk, rbInputBaik, rbInputBuruk, rbOutputBaik, rbOutputBuruk;
    private EditText etDown, etUp, etSnr;

    private List<EditText> texts = new ArrayList<>();
    private List<String> data = new ArrayList<>();

    private ModelHFC model;
    private String SPLITER = "";
    private String GROUND = "";
    private String stDown, stUp, stSnr, stDrop, stTestP, stInput, stOutput;

    public FragmentTestPointHFC() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test_point_hfc, container, false);
        getActivity().setTitle("Check Test Point");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            model = (ModelHFC) bundle.getSerializable("bundle");
            SPLITER = model.getCheckSplitter();
            GROUND = model.getCheckTestPoint();
        } else {
        }

        rgDrop = v.findViewById(R.id.rg_drop_test_point_hfc);
        rbDropBaik = v.findViewById(R.id.rb_baik_drop_test_point_hfc);
        rbDropBuruk = v.findViewById(R.id.rb_buruk_drop_test_point_hfc);

        rgTestP = v.findViewById(R.id.rg_test_point_hfc);
        rbTestPBaik = v.findViewById(R.id.rb_baik_test_point_hfc);
        rbTestPBuruk = v.findViewById(R.id.rb_buruk_test_point_hfc);

        rgInput = v.findViewById(R.id.rg_input_test_point_hfc);
        rbInputBaik = v.findViewById(R.id.rb_baik_input_test_point_hfc);
        rbInputBuruk = v.findViewById(R.id.rb_buruk_input_test_point_hfc);

        rgOutput = v.findViewById(R.id.rg_output_test_point_hfc);
        rbOutputBaik = v.findViewById(R.id.rb_baik_output_test_point_hfc);
        rbOutputBuruk = v.findViewById(R.id.rb_buruk_output_test_point_hfc);

        etDown = v.findViewById(R.id.et_downstream_test_point_hfc);
        etUp = v.findViewById(R.id.et_upstream_test_point_hfc);
        etSnr = v.findViewById(R.id.et_snr_test_point_hfc);

        texts.add(etDown);
        texts.add(etUp);
        texts.add(etSnr);

        stDown = model.getSignalDownstreamPoint();
        stUp = model.getSignalUpstreamPoint();
        stSnr = model.getSignalSNRPoint();

        data.add(stDown);
        data.add(stUp);
        data.add(stSnr);

        fetchData(data);

        btnNext = v.findViewById(R.id.btn_next_test_point_hfc);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int drop = rgDrop.getCheckedRadioButtonId();
                int testP = rgTestP.getCheckedRadioButtonId();
                int input = rgInput.getCheckedRadioButtonId();
                int output = rgOutput.getCheckedRadioButtonId();
                if (drop <= 0) {
                    rgDrop.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Kabel Drop belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (testP <= 0) {
                    rgTestP.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Test Point belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (input <= 0) {
                    rgInput.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Konektor Input belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (output <= 0) {
                    rgOutput.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Konektor Output belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (texts.get(0).getText().toString().isEmpty() ||
                        texts.get(1).getText().toString().isEmpty() ||
                        texts.get(2).getText().toString().isEmpty()) {
                    ValidateHelper vh = new ValidateHelper();
                    vh.validasi(texts);
                } else {
                    if (drop == rbDropBaik.getId()) {
                        stDrop = rbDropBaik.getText().toString(); }
                    if (drop == rbDropBuruk.getId()) {
                        stDrop = rbDropBuruk.getText().toString(); }

                    if (testP == rbTestPBaik.getId()) {
                        stTestP = rbTestPBaik.getText().toString(); }
                    if (testP == rbTestPBuruk.getId()) {
                        stTestP = rbTestPBuruk.getText().toString(); }

                    if (input == rbInputBaik.getId()) {
                        stInput = rbInputBaik.getText().toString(); }
                    if (input == rbInputBuruk.getId()) {
                        stInput = rbInputBuruk.getText().toString(); }

                    if (output == rbOutputBaik.getId()) {
                        stOutput = rbOutputBaik.getText().toString(); }
                    if (output == rbOutputBuruk.getId()) {
                        stOutput = rbOutputBuruk.getText().toString(); }

                    model.setCableDropTestPoint(stDrop);
                    model.setConditionTestPoint(stTestP);
                    model.setConnectorInnPoint(stInput);
                    model.setConnectorOutPoint(stOutput);
                    sendData(model, texts);
                    FragmentTapHFC fth = new FragmentTapHFC();
                    FragmentHelper fh = new FragmentHelper();
                    fh.NET = model.getNetwork();
                    fh.extra(fth, "bundle", model);
                    fh.goToFragment(R.id.ll_fragment_frame_hfc, fth, getFragmentManager());
                }
            }
        });

        btnBack = v.findViewById(R.id.btn_back_test_point_hfc);
        if (SPLITER.equals("y")) {
            backAction(new FragmentSpliterHFC(), model);
        } else if (SPLITER.equals("n")) {
            backAction(new FragmentOutletHFC(), model);
        } else {
        }
    }

    private void backAction(final Fragment f, final ModelHFC m) {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.extra(f, "bundle", m);
                fh.goToFragment(R.id.ll_fragment_frame_hfc, f, getFragmentManager());
            }
        });
    }

    private void fetchData(List<String> str) {
        for (int i = 0; i < str.size(); i++) {
            if (!str.isEmpty()) {
                etDown.setText(str.get(0));
                etUp.setText(str.get(1));
                etSnr.setText(str.get(2));
            } else {
            }
        }
    }

    private void sendData(ModelHFC m, List<EditText> edt) {
        m.setSignalDownstreamPoint(edt.get(0).getText().toString());
        m.setSignalUpstreamPoint(edt.get(1).getText().toString());
        m.setSignalSNRPoint(edt.get(2).getText().toString());
    }
}
