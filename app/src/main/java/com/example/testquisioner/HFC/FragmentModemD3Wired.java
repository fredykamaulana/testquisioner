package com.example.testquisioner.HFC;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.General.FragmentRegisterModem;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;
import com.example.testquisioner.Helper.ValidateHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentModemD3Wired extends Fragment {
    private Button btnNext, btnBack;
    private EditText etBoundDown, etBoundUp, etBoundSnr, etDown, etUp, etSnr;

    private ModelHFC model;
    private String STATE = "";
    private String SELECTED_NET = "";
    private String stBoundDown, stBoundUp, stBoundSnr, stDown, stUp, stSnr;

    private List<EditText> texts = new ArrayList<>();
    private List<String> data = new ArrayList<>();

    public FragmentModemD3Wired() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_modem_d3_wired, container, false);
        getActivity().setTitle("Check Modem D3 Wired");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            model = (ModelHFC) bundle.getSerializable("bundle");
            STATE = model.getState();
            SELECTED_NET = model.getNetwork();
        } else {
        }

        etBoundDown = v.findViewById(R.id.et_bound_downstream_d3wired);
        etBoundUp = v.findViewById(R.id.et_bound_upstream_d3wired);
        etBoundSnr = v.findViewById(R.id.et_bound_snr_d3wired);
        etDown = v.findViewById(R.id.et_downstream_cable_modem_d3wired);
        etUp = v.findViewById(R.id.et_upstream_cable_modem_d3wired);
        etSnr = v.findViewById(R.id.et_snr_cable_modem_d3wired);

        texts.add(etBoundDown);
        texts.add(etBoundUp);
        texts.add(etBoundSnr);
        texts.add(etDown);
        texts.add(etUp);
        texts.add(etSnr);

        stBoundDown = model.getBoundingChannelDownstreamModem();
        stBoundUp = model.getBoundingChannelUpstreamModem();
        stBoundSnr = model.getBoundingChannelSNRModem();
        stDown = model.getSignalDownstreamModem();
        stUp = model.getSignalUpstreamModem();
        stSnr = model.getSignalSNRModem();

        data.add(stBoundDown);
        data.add(stBoundUp);
        data.add(stBoundSnr);
        data.add(stDown);
        data.add(stUp);
        data.add(stSnr);

        if (STATE.equals("before")){
            fetchData(data);
        }else if (STATE.equals("after")){ }

        btnNext = v.findViewById(R.id.btn_next_d3wired);
        if (STATE.equals("after")){
            nextAction(new FragmentRegisterModem(), model, texts);
        }else if (STATE.equals("before")){
            nextAction(new FragmentOutletHFC(), model, texts);
        }else { }

        btnBack = v.findViewById(R.id.btn_back_d3wired);
        if (STATE.equals("before")){
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), MainActivityHFC.class);
                    intent.putExtra("bundle",model);
                    startActivity(intent);
                    getActivity().finish();
                }
            });
        }else if (STATE.equals("after")){
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTapHFC fth = new FragmentTapHFC();
                    FragmentHelper fh = new FragmentHelper();
                    fh.extra(fth, "bundle", model);
                    fh.goToFragment(R.id.ll_fragment_frame_hfc, fth, getFragmentManager());
                }
            });
        }else {}
    }

    private void nextAction(final Fragment f, final ModelHFC model, final List<EditText> txt){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (    txt.get(0).getText().toString().isEmpty() ||
                        txt.get(1).getText().toString().isEmpty() ||
                        txt.get(2).getText().toString().isEmpty() ||
                        txt.get(3).getText().toString().isEmpty() ||
                        txt.get(4).getText().toString().isEmpty() ||
                        txt.get(5).getText().toString().isEmpty() ){
                    ValidateHelper vh = new ValidateHelper();
                    vh.validasi(txt);
                }else {
                    sendData(model,txt);
                    FragmentHelper fh = new FragmentHelper();
                    fh.NET = model.getNetwork();
                    fh.goToFragment(R.id.ll_fragment_frame_hfc, f, getFragmentManager());
                    fh.extra(f, "bundle", model);
                }
            }
        });
    }

    private void fetchData(List<String> str){
        for (int i = 0; i < str.size(); i++) {
            if (!str.isEmpty()){
                etBoundDown.setText(str.get(0));
                etBoundUp.setText(str.get(1));
                etBoundSnr.setText(str.get(2));
                etDown.setText(str.get(3));
                etUp.setText(str.get(4));
                etSnr.setText(str.get(5));
            }else { }
        }
    }

    private void sendData(ModelHFC m, List<EditText> edt){
        m.setBoundingChannelDownstreamModem(edt.get(0).getText().toString());
        m.setBoundingChannelUpstreamModem(edt.get(1).getText().toString());
        m.setBoundingChannelSNRModem(edt.get(2).getText().toString());
        m.setSignalDownstreamModem(edt.get(3).getText().toString());
        m.setSignalUpstreamModem(edt.get(4).getText().toString());
        m.setSignalSNRModem(edt.get(5).getText().toString());
    }
}
