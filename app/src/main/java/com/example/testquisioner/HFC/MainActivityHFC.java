package com.example.testquisioner.HFC;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.General.MainActivityNetwork;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;

public class MainActivityHFC extends AppCompatActivity {
    private boolean FLAG_ANSWER = false;
    private Spinner spDevice, spDeviceType;
    private Button btnNext, btnBack;
    private LinearLayout llSetDevice;

    private String SELECTED_DEVICE = "";
    private String SELECTED_TYPE = "";
    private String SELECTED_NET = "";
    private int DIALOG_COUNT = 0;

    private ModelHFC modelHFC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_hfc);
        setTitle("Jaringan HFC");
        setupView();
        setupAction();
        setupDialog();
    }

    private void setupAction() {
        final Intent intent = getIntent();
        if (intent != null){
            modelHFC = (ModelHFC) intent.getSerializableExtra("bundle");
            SELECTED_NET = modelHFC.getNetwork();
        }

        final ArrayAdapter<String> deviceAdapter, modemAdapter, stbAdapter;

        deviceAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.device));
        deviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        modemAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.modem_type));
        modemAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        stbAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.stb_type));
        stbAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spDevice.setAdapter(deviceAdapter);
        spDevice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_DEVICE = spDevice.getSelectedItem().toString();
                modelHFC.setDevice(SELECTED_DEVICE);
                if (SELECTED_DEVICE.equals("Modem")) {
                    spDeviceType.setAdapter(modemAdapter);
                } else if (SELECTED_DEVICE.equals("STB")) {
                    spDeviceType.setAdapter(stbAdapter);
                } else {
                    spDeviceType.setAdapter(null);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spDeviceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_TYPE = spDeviceType.getSelectedItem().toString();
                modelHFC.setType(SELECTED_TYPE);
                modelHFC.setState("before");
                if (SELECTED_TYPE.equals("") || SELECTED_TYPE.equals("--Select Type--") ) {

                } else {
                    btnNext.setClickable(true);
                    if (SELECTED_TYPE.equals("Modem D3 Wifi")) {
                        nextAction(new FragmentModemD3Wifi(), modelHFC);
                    } else if (SELECTED_TYPE.equals("Modem D3 Wired")) {
                        nextAction(new FragmentModemD3Wired(), modelHFC);
                    } else if (SELECTED_TYPE.equals("Modem D2 Wifi")) {
                        nextAction(new FragmentModemD2Wifi(), modelHFC);
                    } else if (SELECTED_TYPE.equals("Modem D2 Wired")) {
                        nextAction(new FragmentModemD2Wired(), modelHFC);
                    } else if (SELECTED_TYPE.equals("Smartbox")) {
                        nextAction(new FragmentSmartbox(), modelHFC);
                    } else if (SELECTED_TYPE.equals("STB Lite")) {
                        nextAction(new FragmentSTBLite(), modelHFC);
                    } else {
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(MainActivityHFC.this, MainActivityNetwork.class);
                main.putExtra("bundle", modelHFC);
                startActivity(main);
                finish();
            }
        });
    }

    private void setupView() {
        spDevice = findViewById(R.id.sp_device);
        spDeviceType = findViewById(R.id.sp_device_type);
        btnNext = findViewById(R.id.btn_next_hfc);
        btnBack = findViewById(R.id.btn_back_hfc);
        llSetDevice = findViewById(R.id.ll_set_device);
        llSetDevice.setVisibility(View.GONE);
    }

    private boolean setupDialog() {
        DIALOG_COUNT++;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Apakah Modem/STB Menyala ?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FLAG_ANSWER = true;
                llSetDevice.setVisibility(View.VISIBLE);
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FLAG_ANSWER = false;
                if (DIALOG_COUNT == 1) {
                    giveAdvice("Silahkan Ganti Adaptor");
                } else if (DIALOG_COUNT == 2) {
                    giveAdvice("Silahkan Ganti Modem");
                } else {
                }
            }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();

        return FLAG_ANSWER;
    }

    private void giveAdvice(String s) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(s);
        builder.setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (DIALOG_COUNT < 2) {
                    setupDialog();
                } else if (DIALOG_COUNT == 2) {
                    llSetDevice.setVisibility(View.VISIBLE);
                } else {
                }
            }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    private void nextAction(final Fragment f, final ModelHFC m){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llSetDevice.setVisibility(View.GONE);
                FragmentHelper fh = new FragmentHelper();
                fh.goToFragment(R.id.ll_fragment_frame_hfc, f, getSupportFragmentManager());
                fh.extra(f, "bundle", m);
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
