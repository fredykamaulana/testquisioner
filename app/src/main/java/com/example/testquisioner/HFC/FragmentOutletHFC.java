package com.example.testquisioner.HFC;


import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.testquisioner.Helper.DialogHelper;
import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;

import java.io.Serializable;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOutletHFC extends Fragment {
    private Button btnNext, btnBack;
    private RadioGroup rgCoax, rgWallplate, rgKonektor;
    private RadioButton rbCoaxBaik, rbCoaxBuruk, rbWallplateBaik, rbWallplateBuruk, rbKonektorBaik, rbKonektorBuruk;

    private ModelHFC model;
    private String SELECTED_TYPE = "";
    private String stCoax, stWallplate, stKonektor;

    public FragmentOutletHFC() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_outlet_hfc, container, false);
        getActivity().setTitle("Check Instalasi Outlet");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            model = (ModelHFC) bundle.getSerializable("bundle");
            SELECTED_TYPE = model.getType();
        } else {
        }

        rgCoax = v.findViewById(R.id.rg_kondisi_coax_hfc);
        rbCoaxBaik = v.findViewById(R.id.rb_baik_coax_hfc);
        rbCoaxBuruk = v.findViewById(R.id.rb_buruk_coax_hfc);

        rgWallplate = v.findViewById(R.id.rg_wallplate_hfc);
        rbWallplateBaik = v.findViewById(R.id.rb_baik_wallplate_hfc);
        rbWallplateBuruk = v.findViewById(R.id.rb_buruk_wallplate_hfc);

        rgKonektor = v.findViewById(R.id.rg_konektor_hfc);
        rbKonektorBaik = v.findViewById(R.id.rb_baik_konektor_hfc);
        rbKonektorBuruk = v.findViewById(R.id.rb_buruk_konektor_hfc);

        btnNext = v.findViewById(R.id.btn_next_outlet_hfc);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int coax = rgCoax.getCheckedRadioButtonId();
                int wallplate = rgWallplate.getCheckedRadioButtonId();
                int konektor = rgKonektor.getCheckedRadioButtonId();
                if (coax <= 0) {
                    rgCoax.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Kabel Coax belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (wallplate <= 0) {
                    rgWallplate.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Wallplate belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (konektor <= 0) {
                    rgKonektor.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Konektor belum dicheck", Toast.LENGTH_SHORT).show();
                } else {
                    if (coax == rbCoaxBaik.getId()) {
                        stCoax = rbCoaxBaik.getText().toString();
                        model.setCoaxOutlet(stCoax); }
                    if (coax == rbCoaxBuruk.getId()) {
                        stCoax = rbCoaxBuruk.getText().toString();
                        model.setCoaxOutlet(stCoax); }

                    if (wallplate == rbWallplateBaik.getId()) {
                        stWallplate = rbWallplateBaik.getText().toString();
                        model.setWallplateOutlet(stWallplate); }
                    if (wallplate == rbWallplateBuruk.getId()) {
                        stWallplate = rbWallplateBuruk.getText().toString();
                        model.setWallplateOutlet(stWallplate); }

                    if (konektor == rbKonektorBaik.getId()) {
                        stKonektor = rbKonektorBaik.getText().toString();
                        model.setConecctorInputtoModem(stKonektor); }
                    if (konektor == rbKonektorBuruk.getId()) {
                        stKonektor = rbKonektorBuruk.getText().toString();
                        model.setConecctorInputtoModem(stKonektor); }

                    newDialog(model);
                }
            }
        });

        btnBack = v.findViewById(R.id.btn_back_outlet_hfc);
        model.setState("before");
        if (SELECTED_TYPE.equals("Modem D3 Wifi")) {
            backAction(new FragmentModemD3Wifi(), model);
        } else if (SELECTED_TYPE.equals("Modem D3 Wired")) {
            backAction(new FragmentModemD3Wired(), model);
        } else if (SELECTED_TYPE.equals("Modem D2 Wifi")) {
            backAction(new FragmentModemD2Wifi(), model);
        } else if (SELECTED_TYPE.equals("Modem D2 Wired")) {
            backAction(new FragmentModemD2Wired(), model);
        } else if (SELECTED_TYPE.equals("Smartbox")) {
            backAction(new FragmentSmartbox(), model);
        } else if (SELECTED_TYPE.equals("STB Lite")) {
            backAction(new FragmentSTBLite(), model);
        } else {
            Toast.makeText(getContext(), "Parameter Lost", Toast.LENGTH_SHORT).show();
        }
    }

    private void newDialog(final ModelHFC m) {
        DialogHelper dh = new DialogHelper();
        dh.setupDialog(new AlertDialog.Builder(getContext()), "Apakah Splitter bisa diakses ?",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        m.setCheckSplitter("y");
                        //positive
                        //ke layout splitter
                        nextAction(new FragmentSpliterHFC(), m);
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        m.setCheckSplitter("n");
                        //negative
                        //make dialog again
                        DialogHelper dh = new DialogHelper();
                        dh.setupDialog(new AlertDialog.Builder(getContext()), "Apakah Ground dapat diakses ?",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        m.setCheckTestPoint("y");
                                        //positive
                                        //go to test point
                                        nextAction(new FragmentTestPointHFC(), m);
                                    }
                                },
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        m.setCheckTestPoint("n");
                                        //negative
                                        //go to TAP
                                        nextAction(new FragmentTapHFC(), m);
                                    }
                                });
                    }
                }
        );
    }

    private void backAction(final Fragment fragment, final Serializable model) {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.goToFragment(R.id.ll_fragment_frame_hfc, fragment, getFragmentManager());
                fh.extra(fragment, "bundle", model);
            }
        });
    }

    private void nextAction(final Fragment f, final ModelHFC m) {
        FragmentHelper fh = new FragmentHelper();
        fh.extra(f, "bundle", m);
        fh.goToFragment(R.id.ll_fragment_frame_hfc, f, getFragmentManager());
    }
}
