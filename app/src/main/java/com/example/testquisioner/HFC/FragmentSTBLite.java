package com.example.testquisioner.HFC;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.General.FragmentRegisterModem;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;
import com.example.testquisioner.Helper.ValidateHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSTBLite extends Fragment {
    private Button btnNext, btnBack;
    private ImageButton btnTambah, btnKurang;
    private LinearLayout channel1, channel2;
    private EditText etNumChannel1, etPower1, etQuality1, etCn1, etBer1;
    private EditText etNumChannel2, etPower2, etQuality2, etCn2, etBer2;
    private EditText etNumChannel3, etPower3, etQuality3, etCn3, etBer3;

    private ModelHFC model;
    private String STATE = "";
    private String SELECTED_NET = "";
    private int CHANNEL = 1;

    private List<EditText> texts = new ArrayList<>();

    public FragmentSTBLite() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stblite, container, false);
        getActivity().setTitle("Check STB Lite");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            model = (ModelHFC) bundle.getSerializable("bundle");
            STATE = model.getState();
            SELECTED_NET = model.getNetwork();
        } else {
        }

        etNumChannel1 = v.findViewById(R.id.et_channel_check_signal_stb_lite);
        etPower1 = v.findViewById(R.id.et_strength_power_stb_lite);
        etQuality1 = v.findViewById(R.id.et_strength_quality_stb_lite);
        etCn1 = v.findViewById(R.id.et_nilai_cn_stb_lite);
        etBer1 = v.findViewById(R.id.et_nilai_ber_stb_lite);

        texts.add(etNumChannel1);
        texts.add(etPower1);
        texts.add(etQuality1);
        texts.add(etCn1);
        texts.add(etBer1);

        etNumChannel2 = v.findViewById(R.id.et_channel_check_signal_stb_lite_1);
        etPower2 = v.findViewById(R.id.et_strength_power_stb_lite_1);
        etQuality2 = v.findViewById(R.id.et_strength_quality_stb_lite_1);
        etCn2 = v.findViewById(R.id.et_nilai_cn_stb_lite_1);
        etBer2 = v.findViewById(R.id.et_nilai_ber_stb_lite_1);

        texts.add(etNumChannel2);
        texts.add(etPower2);
        texts.add(etQuality2);
        texts.add(etCn2);
        texts.add(etBer2);

        etNumChannel3 = v.findViewById(R.id.et_channel_check_signal_stb_lite_2);
        etPower3 = v.findViewById(R.id.et_strength_power_stb_lite_2);
        etQuality3 = v.findViewById(R.id.et_strength_quality_stb_lite_2);
        etCn3 = v.findViewById(R.id.et_nilai_cn_stb_lite_2);
        etBer3 = v.findViewById(R.id.et_nilai_ber_stb_lite_2);

        texts.add(etNumChannel3);
        texts.add(etPower3);
        texts.add(etQuality3);
        texts.add(etCn3);
        texts.add(etBer3);

        channel1 = v.findViewById(R.id.ll_check_stblite_1);
        channel2 = v.findViewById(R.id.ll_check_stblite_2);

        btnNext = v.findViewById(R.id.btn_next_stb_lite);
        if (STATE.equals("after")){
            nextAction(new FragmentRegisterModem(), model, texts, SELECTED_NET);
        }else if (STATE.equals("before")){
            nextAction(new FragmentOutletHFC(), model, texts, SELECTED_NET);
        }else { }

        btnBack = v.findViewById(R.id.btn_back_stb_lite);
        if (STATE.equals("before")){
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), MainActivityHFC.class);
                    intent.putExtra("bundle",model);
                    startActivity(intent);
                    getActivity().finish();
                }
            });
        }else if (STATE.equals("after")){
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTapHFC fth = new FragmentTapHFC();
                    FragmentHelper fh = new FragmentHelper();
                    fh.NET = SELECTED_NET;
                    fh.extra(fth, "bundle", model);
                    fh.goToFragment(R.id.ll_fragment_frame_hfc, fth, getFragmentManager());
                }
            });
        }else {}

        btnTambah = v.findViewById(R.id.btn_tambah_channel_stblite);
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CHANNEL == 1){
                    channel1.setVisibility(View.VISIBLE);
                    channel1.setAlpha(0.0f);
                    channel1.animate().alpha(1.0f).setDuration(1000);
                    btnKurang.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(), "Check channel 2 telah ditambahkan", Toast.LENGTH_SHORT).show();
                    CHANNEL ++;
                }else if (CHANNEL == 2){
                    channel2.setVisibility(View.VISIBLE);
                    channel2.setAlpha(0.0f);
                    channel2.animate().alpha(1.0f).setDuration(1000);
                    Toast.makeText(getContext(), "Check channel 3 telah ditambahkan", Toast.LENGTH_SHORT).show();
                    CHANNEL++;
                }else { }
            }
        });

        btnKurang = v.findViewById(R.id.btn_kurang_channel_stblite);
        btnKurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CHANNEL == 3){
                    channel2.setAlpha(1.0f);
                    channel2.animate().alpha(0.0f).setDuration(2000);
                    channel2.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Check channel 3 telah dihapus", Toast.LENGTH_SHORT).show();
                    CHANNEL --;
                }else if (CHANNEL == 2){
                    channel1.setAlpha(1.0f);
                    channel1.animate().alpha(0.0f).setDuration(2000);
                    channel1.setVisibility(View.GONE);
                    btnKurang.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Check channel 2 telah dihapus", Toast.LENGTH_SHORT).show();
                    CHANNEL--;
                }else { }
            }
        });
    }

    private void nextAction(final Fragment f, final ModelHFC model, final List<EditText> txt, final String s){
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int ch = CHANNEL;
                if (ch > 0){
                    if (ch == 1){
                        if (    txt.get(0).getText().toString().isEmpty() ||
                                txt.get(1).getText().toString().isEmpty() ||
                                txt.get(2).getText().toString().isEmpty() ||
                                txt.get(3).getText().toString().isEmpty() ||
                                txt.get(4).getText().toString().isEmpty() ){
                            ValidateHelper vh = new ValidateHelper();
                            vh.validasi(txt);
                        }else {
                            sendToModelFTTH(model, txt);
                            FragmentHelper fh = new FragmentHelper();
                            fh.NET = s;
                            fh.goToFragment(R.id.ll_fragment_frame_hfc, f, getFragmentManager());
                            fh.extra(f, "bundle", model);
                        }
                    }else if (ch == 2){
                        if (    txt.get(0).getText().toString().isEmpty() ||
                                txt.get(1).getText().toString().isEmpty() ||
                                txt.get(2).getText().toString().isEmpty() ||
                                txt.get(3).getText().toString().isEmpty() ||
                                txt.get(4).getText().toString().isEmpty() ||
                                txt.get(5).getText().toString().isEmpty() ||
                                txt.get(6).getText().toString().isEmpty() ||
                                txt.get(7).getText().toString().isEmpty() ||
                                txt.get(8).getText().toString().isEmpty() ||
                                txt.get(9).getText().toString().isEmpty() ){
                            ValidateHelper vh = new ValidateHelper();
                            vh.validasi(txt);
                        }else {
                            sendToModelFTTH(model, txt);
                            FragmentHelper fh = new FragmentHelper();
                            fh.NET = s;
                            fh.goToFragment(R.id.ll_fragment_frame_hfc, f, getFragmentManager());
                            fh.extra(f, "bundle", model);
                        }
                    }else if (ch == 3){
                        if (    txt.get(0).getText().toString().isEmpty() ||
                                txt.get(1).getText().toString().isEmpty() ||
                                txt.get(2).getText().toString().isEmpty() ||
                                txt.get(3).getText().toString().isEmpty() ||
                                txt.get(4).getText().toString().isEmpty() ||
                                txt.get(5).getText().toString().isEmpty() ||
                                txt.get(6).getText().toString().isEmpty() ||
                                txt.get(7).getText().toString().isEmpty() ||
                                txt.get(8).getText().toString().isEmpty() ||
                                txt.get(9).getText().toString().isEmpty() ||
                                txt.get(10).getText().toString().isEmpty() ||
                                txt.get(11).getText().toString().isEmpty() ||
                                txt.get(12).getText().toString().isEmpty() ||
                                txt.get(13).getText().toString().isEmpty() ||
                                txt.get(14).getText().toString().isEmpty() ){
                            ValidateHelper vh = new ValidateHelper();
                            vh.validasi(txt);
                        }else {
                            sendToModelFTTH(model, txt);
                            FragmentHelper fh = new FragmentHelper();
                            fh.NET = s;
                            fh.goToFragment(R.id.ll_fragment_frame_hfc, f, getFragmentManager());
                            fh.extra(f, "bundle", model);
                        }
                    }
                }
            }
        });
    }

    private void sendToModelFTTH(ModelHFC m, List<EditText> edt){
        m.setChannelSTB1(edt.get(0).getText().toString());
        m.setStrengthPowerSTB1(edt.get(1).getText().toString());
        m.setStrengthQualitySTB1(edt.get(2).getText().toString());
        m.setCnSTB1(edt.get(3).getText().toString());
        m.setBerSTB1(edt.get(4).getText().toString());
        m.setChannelSTB2(edt.get(5).getText().toString());
        m.setStrengthPowerSTB2(edt.get(6).getText().toString());
        m.setStrengthQualitySTB2(edt.get(7).getText().toString());
        m.setCnSTB2(edt.get(8).getText().toString());
        m.setBerSTB2(edt.get(9).getText().toString());
        m.setChannelSTB3(edt.get(10).getText().toString());
        m.setStrengthPowerSTB3(edt.get(11).getText().toString());
        m.setStrengthQualitySTB3(edt.get(12).getText().toString());
        m.setCnSTB3(edt.get(13).getText().toString());
        m.setBerSTB3(edt.get(14).getText().toString());
    }
}
