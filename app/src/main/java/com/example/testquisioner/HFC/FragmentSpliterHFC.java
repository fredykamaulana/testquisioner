package com.example.testquisioner.HFC;


import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


import com.example.testquisioner.Helper.DialogHelper;
import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;
import com.example.testquisioner.Helper.ValidateHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSpliterHFC extends Fragment {
    private Button btnNext, btnBack;
    private RadioGroup rgCoax, rgSplitter, rgInput, rgOutput;
    private RadioButton rbCoaxBaik, rbCoaxBuruk, rbSplitterBaik, rbSplitterBuruk, rbInputBaik, rbInputBuruk, rbOutputBaik, rbOutputBuruk;
    private EditText etDown, etUp, etSnr;

    private ModelHFC model;
    private String stDown, stUp, stSnr, stCoax, stSplitter, stInput, stOutput;

    private List<EditText> texts = new ArrayList<>();
    private List<String> data = new ArrayList<>();

    public FragmentSpliterHFC() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_spliter_hfc, container, false);
        getActivity().setTitle("Check Instalasi Spliter");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            model = (ModelHFC) bundle.getSerializable("bundle");
        } else {
        }

        rgCoax = v.findViewById(R.id.rg_kondisi_coax_splitter_hfc);
        rbCoaxBaik = v.findViewById(R.id.rb_baik_coax_splitter_hfc);
        rbCoaxBuruk = v.findViewById(R.id.rb_buruk_coax_splitter_hfc);

        rgSplitter = v.findViewById(R.id.rg_splitter_hfc);
        rbSplitterBaik = v.findViewById(R.id.rb_baik_splitter_hfc);
        rbSplitterBuruk = v.findViewById(R.id.rb_buruk_splitter_hfc);

        rgInput = v.findViewById(R.id.rg_input_splitter_hfc);
        rbInputBaik = v.findViewById(R.id.rb_baik_input_splitter_hfc);
        rbInputBuruk = v.findViewById(R.id.rb_buruk_output_splitter_hfc);

        rgOutput = v.findViewById(R.id.rg_output_splitter_hfc);
        rbOutputBaik = v.findViewById(R.id.rb_baik_output_splitter_hfc);
        rbOutputBuruk = v.findViewById(R.id.rb_buruk_output_splitter_hfc);

        etDown = v.findViewById(R.id.et_downstream_splitter_hfc);
        etUp = v.findViewById(R.id.et_upstream_splitter_hfc);
        etSnr = v.findViewById(R.id.et_snr_splitter_hfc);

        texts.add(etDown);
        texts.add(etUp);
        texts.add(etSnr);

        stDown = model.getSignalDownstreamSplitter();
        stUp = model.getSignalUpstreamSplitter();
        stSnr = model.getSignalSNRSplitter();

        data.add(stDown);
        data.add(stUp);
        data.add(stSnr);

        fetchData(data);

        btnNext = v.findViewById(R.id.btn_next_splitter_hfc);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //send data to modelhfc
                int coax = rgCoax.getCheckedRadioButtonId();
                int splitter = rgSplitter.getCheckedRadioButtonId();
                int input = rgInput.getCheckedRadioButtonId();
                int output = rgOutput.getCheckedRadioButtonId();
                if (coax <= 0) {
                    rgCoax.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Kabel Coax belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (splitter <= 0) {
                    rgSplitter.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Splitter belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (input <= 0) {
                    rgInput.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Konektor Input belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (output <= 0) {
                    rgOutput.requestFocus();
                    Toast.makeText(getContext(), "Kondisi Konektor Output belum dicheck", Toast.LENGTH_SHORT).show();
                } else if (texts.get(0).getText().toString().isEmpty() ||
                        texts.get(1).getText().toString().isEmpty() ||
                        texts.get(2).getText().toString().isEmpty()) {
                    ValidateHelper vh = new ValidateHelper();
                    vh.validasi(texts);
                } else {
                    if (coax == rbCoaxBaik.getId()) {
                        stCoax = rbCoaxBaik.getText().toString();
                        model.setCoaxSplitter(stCoax); }
                    if (coax == rbCoaxBuruk.getId()) {
                        stCoax = rbCoaxBuruk.getText().toString();
                        model.setCoaxSplitter(stCoax); }

                    if (splitter == rbSplitterBaik.getId()) {
                        stSplitter = rbSplitterBaik.getText().toString();
                        model.setConditionSplitter(stSplitter); }
                    if (splitter == rbSplitterBuruk.getId()) {
                        stSplitter = rbSplitterBuruk.getText().toString();
                        model.setConditionSplitter(stSplitter); }

                    if (input == rbInputBaik.getId()) {
                        stInput = rbInputBaik.getText().toString();
                        model.setConnectorInnSplitter(stInput); }
                    if (input == rbInputBuruk.getId()) {
                        stInput = rbInputBuruk.getText().toString();
                        model.setConnectorInnSplitter(stInput); }

                    if (output == rbOutputBaik.getId()) {
                        stOutput = rbOutputBaik.getText().toString();
                        model.setConnectorOutSplitter(stOutput); }
                    if (output == rbOutputBuruk.getId()) {
                        stOutput = rbOutputBuruk.getText().toString();
                        model.setConnectorOutSplitter(stOutput); }

                    startDialog(model, texts);
                }
            }
        });
        btnBack = v.findViewById(R.id.btn_back_splitter_hfc);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentOutletHFC foh = new FragmentOutletHFC();
                FragmentHelper fh = new FragmentHelper();
                fh.extra(foh, "bundle", model);
                fh.goToFragment(R.id.ll_fragment_frame_hfc, foh, getFragmentManager());
            }
        });
    }

    private boolean startDialog(final ModelHFC m, final List<EditText> txt) {
        DialogHelper dh = new DialogHelper();
        dh.setupDialog(new AlertDialog.Builder(getContext()),
                "Apakah Ground bisa diakses ?",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //positive
                        m.setCheckTestPoint("y");
                        //to test point
                        sendData(m, txt);
                        nextAction(new FragmentTestPointHFC(), m);
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //negative
                        m.setCheckTestPoint("n");
                        //to test TAP
                        sendData(m, txt);
                        nextAction(new FragmentTapHFC(), m);
                    }
                });

        return true;
    }

    private void nextAction(final Fragment f, final ModelHFC m) {
        FragmentHelper fh = new FragmentHelper();
        fh.extra(f, "bundle", m);
        fh.goToFragment(R.id.ll_fragment_frame_hfc, f, getFragmentManager());
    }


    private void fetchData(List<String> str) {
        for (int i = 0; i < str.size(); i++) {
            if (!str.isEmpty()) {
                etDown.setText(str.get(0));
                etUp.setText(str.get(1));
                etSnr.setText(str.get(2));
            } else {
            }
        }
    }

    private void sendData(ModelHFC m, List<EditText> edt) {
        m.setSignalDownstreamSplitter(edt.get(0).getText().toString());
        m.setSignalUpstreamSplitter(edt.get(1).getText().toString());
        m.setSignalSNRSplitter(edt.get(2).getText().toString());
    }
}
