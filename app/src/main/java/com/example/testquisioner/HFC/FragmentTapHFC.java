package com.example.testquisioner.HFC;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.testquisioner.Helper.FragmentHelper;
import com.example.testquisioner.Model.ModelHFC;
import com.example.testquisioner.R;
import com.example.testquisioner.Helper.ValidateHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTapHFC extends Fragment {
    private Button btnNext, btnBack;
    private RadioGroup rgFKonektor, rgFullTap;
    private RadioButton rbKonektorBaik, rbKonektorBuruk, rbTapYa, rbTapTdk;
    private EditText etDown, etUp, etSnr, etValTap, etDrop;

    private List<EditText> texts = new ArrayList<>();
    private List<String> data = new ArrayList<>();

    private ModelHFC model;
    private String SELECTED_TYPE = "";
    private String SPLITER = "";
    private String GROUND = "";
    private String stDown, stUp, stSnr, stValTap, stDrop, stKonektor, stFullTap;

    public FragmentTapHFC() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tap_hfc, container, false);
        getActivity().setTitle("Check Instalasi TAP");
        setupView(view);
        return view;
    }

    private void setupView(View v) {
        Bundle bundle = getArguments();
        if (bundle != null){
            model = (ModelHFC) bundle.getSerializable("bundle");
            SELECTED_TYPE = model.getType();
            GROUND = model.getCheckTestPoint();
            SPLITER = model.getCheckSplitter();
        }else { }

        rgFKonektor = v.findViewById(R.id.rg_f_connector_tap_hfc);
        rbKonektorBaik = v.findViewById(R.id.rb_baik_f_connector_tap_hfc);
        rbKonektorBuruk = v.findViewById(R.id.rb_buruk_f_connector_tap_hfc);

        rgFullTap = v.findViewById(R.id.rg_full_tap_hfc);
        rbTapYa = v.findViewById(R.id.rb_ya_full_tap_hfc);
        rbTapTdk = v.findViewById(R.id.rb_tdk_full_tap_hfc);

        etDown = v.findViewById(R.id.et_downstream_tap_hfc);
        etUp = v.findViewById(R.id.et_upstream_tap_hfc);
        etSnr = v.findViewById(R.id.et_snr_tap_hfc);
        etValTap = v.findViewById(R.id.et_value_tap_hfc);
        etDrop = v.findViewById(R.id.et_jumlah_drop_tap_hfc);

        texts.add(etDown);
        texts.add(etUp);
        texts.add(etSnr);
        texts.add(etValTap);
        texts.add(etDrop);

        stDown = model.getSignalDownstreamTap();
        stUp = model.getSignalUpstreamTap();
        stSnr = model.getSignalSNRTap();
        stValTap = model.getTapValue();
        stDrop = model.getCableDropToTap();

        data.add(stDown);
        data.add(stUp);
        data.add(stSnr);
        data.add(stValTap);
        data.add(stDrop);

        fetchData(data);

        btnNext = v.findViewById(R.id.btn_next_tap_hfc);
        if (SELECTED_TYPE.equals("Modem D3 Wifi")) {
            nextAction(new FragmentModemD3Wifi(), model, texts);
        } else if (SELECTED_TYPE.equals("Modem D3 Wired")) {
            nextAction(new FragmentModemD3Wired(), model, texts);
        } else if (SELECTED_TYPE.equals("Modem D2 Wifi")) {
            nextAction(new FragmentModemD2Wifi(), model, texts);
        } else if (SELECTED_TYPE.equals("Modem D2 Wired")) {
            nextAction(new FragmentModemD2Wired(), model, texts);
        } else if (SELECTED_TYPE.equals("Smartbox")) {
            nextAction(new FragmentSmartbox(), model, texts);
        } else if (SELECTED_TYPE.equals("STB Lite")) {
            nextAction(new FragmentSTBLite(), model, texts);
        } else {
        }

        btnBack = v.findViewById(R.id.btn_back_tap_hfc);
        if (GROUND.equals("y")){
            //go to test point
            backAction(new FragmentTestPointHFC(), model);
        }else if (GROUND.equals("n")){
            if (SPLITER.equals("y")){
                //go to splitter
                backAction(new FragmentSpliterHFC(),model);
            }else if (SPLITER.equals("n")){
                //go to outlet
                backAction(new FragmentOutletHFC(), model);
            } else {}
        }else {}
    }

    private void nextAction(final Fragment f, final ModelHFC model, final List<EditText> txt){
        model.setState("after");
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int konektor = rgFKonektor.getCheckedRadioButtonId();
                int tap = rgFullTap.getCheckedRadioButtonId();
                if (konektor <= 0 ){
                    rgFKonektor.requestFocus();
                    Toast.makeText(getContext(), "Kondisi F Connector belum dicheck", Toast.LENGTH_SHORT).show();
                }else if (tap <= 0){
                    rgFullTap.requestFocus();
                    Toast.makeText(getContext(), "Tap Full belum dicheck", Toast.LENGTH_SHORT).show();
                }else if (  txt.get(0).getText().toString().isEmpty() ||
                            txt.get(1).getText().toString().isEmpty() ||
                            txt.get(2).getText().toString().isEmpty() ||
                            txt.get(3).getText().toString().isEmpty() ||
                            txt.get(4).getText().toString().isEmpty() ){
                    ValidateHelper vh = new ValidateHelper();
                    vh.validasi(txt);
                }else {
                    if (konektor == rbKonektorBaik.getId()){
                        stKonektor = rbKonektorBaik.getText().toString();
                        model.setConditionFConnectorTap(stKonektor);
                    }if (konektor == rbKonektorBuruk.getId()){
                        stKonektor = rbKonektorBuruk.getText().toString();
                        model.setConditionFConnectorTap(stKonektor); }

                    if (tap == rbTapYa.getId()){
                        stFullTap = rbTapYa.getText().toString();
                        model.setTapFull(stFullTap);
                    }if (tap == rbTapTdk.getId()){
                        stFullTap = rbTapTdk.getText().toString();
                        model.setTapFull(stFullTap); }

                    sendData(model, txt);
                    FragmentHelper fh = new FragmentHelper();
                    fh.NET = model.getNetwork();
                    fh.extra(f, "bundle", model);
                    fh.goToFragment(R.id.ll_fragment_frame_hfc, f, getFragmentManager());
                }
            }
        });
    }

    private void backAction(final Fragment f, final ModelHFC m){
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper fh = new FragmentHelper();
                fh.extra(f,"bundle", m);
                fh.goToFragment(R.id.ll_fragment_frame_hfc, f, getFragmentManager());
            }
        });
    }

    private void fetchData(List<String> str){
        for (int i = 0; i < str.size(); i++) {
            if (!str.isEmpty()){
                etDown.setText(str.get(0));
                etUp.setText(str.get(1));
                etSnr.setText(str.get(2));
                etValTap.setText(str.get(3));
                etDrop.setText(str.get(4));
            }else { }
        }
    }

    private void sendData(ModelHFC m, List<EditText> edt){
        m.setSignalDownstreamTap(edt.get(0).getText().toString());
        m.setSignalUpstreamTap(edt.get(1).getText().toString());
        m.setSignalSNRTap(edt.get(2).getText().toString());
        m.setTapValue(edt.get(3).getText().toString());
        m.setCableDropToTap(edt.get(4).getText().toString());
    }
}
