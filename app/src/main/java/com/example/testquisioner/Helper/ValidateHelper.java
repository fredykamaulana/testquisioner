package com.example.testquisioner.Helper;

import android.widget.EditText;

import java.util.List;

public class ValidateHelper {
    public boolean validasi(List<EditText> list) {
        for (int i = 0; i < list.size(); i++) {
            String value = list.get(i).getText().toString();
            if (value.isEmpty()) {
                list.get(i).setError("Field tidak boleh kosong");
                list.get(i).requestFocus();
                break;
            }
            continue;
        }
        return true;
    }
}
