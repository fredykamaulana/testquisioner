package com.example.testquisioner.Helper;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.io.Serializable;

public class FragmentHelper {
    public String NET = "";
    public void goToFragment(int from, Fragment to, FragmentManager fm){
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(from, to);
        transaction.commit();
    }

    public void extra(Fragment f, String key, Serializable model){
        Bundle bundle = new Bundle();
        bundle.putSerializable(key, model);
        bundle.putString("net", NET);
        f.setArguments(bundle);

    }
}
