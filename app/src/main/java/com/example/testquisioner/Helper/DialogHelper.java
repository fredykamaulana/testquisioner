package com.example.testquisioner.Helper;

import androidx.appcompat.app.AlertDialog;

public class DialogHelper {
    public void setupDialog(AlertDialog.Builder builder, final String s, AlertDialog.OnClickListener positive, AlertDialog.OnClickListener negative){
        builder.setTitle(s);
        builder.setPositiveButton("Ya",positive);
        builder.setNegativeButton("Tidak", negative);
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }
}
